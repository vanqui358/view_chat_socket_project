/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { Text, View, FlatList, TouchableOpacity } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import { useTranslation } from "react-i18next";
import { SafeAreaView } from "react-native-safe-area-context";

import ChatContactIcon from "assets/images/svg/IconNewContactCustomColor.svg";
import styles from "assets/styles/views/ChatHistory/chatHistoryStyle";

import { SCREENS } from "app/constants";
import Header from "components/App/SearchBarHeader.tsx";
import ChatHistoryRow from "./components/ChatHistoryRow";
import NoHistory from "components/ChatHistory/NoHistory";
// import { MockData } from "./DummyData";
import { Button, TouchableRipple } from "react-native-paper";
import IconArchive from "assets/images/svg/IconArchiveSolid.svg";
import { primaryColor } from "assets/styles/theme";
import {
  selectArchives,
  selectUnArchives,
  fetchConversations,
  selectRefreshing,
  refetchContactAndUsers,
} from "store/slices/chatSlice";

const ChatHistory = () => {
  const { navigate } = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const rowRefs = React.useRef([]);
  const openRowKey = React.useRef(null);
  const userRef = React.useRef([]);

  // Selectors
  const isRefreshing = useSelector(selectRefreshing);
  const conversations = useSelector(selectUnArchives);
  const archives = useSelector((state) => selectArchives(state));
  const [listConversation, setListConversation] = React.useState(conversations);
  const [lengthListConversation, setLengthListConversation] = React.useState(
    listConversation.length
  );

  useEffect(() => {
    setListConversation(conversations);
    setLengthListConversation(listConversation.length);
  }, [conversations]);
  useEffect(() => {
    dispatch(fetchConversations());
  }, [lengthListConversation]);
  // use state
  const [isLoading, setIsLoading] = useState(isRefreshing);

  // useEffect
  // useEffect(() => {
  //   // dispatch(
  //   //   upArchives(lodash.filter(chatList, (item) => item.archive === true))
  //   // );
  //   // dispatch(
  //   //   upUnArchives(lodash.filter(chatList, (item) => item.archive === false))
  //   // );
  // }, [chatList, dispatch]);

  const onRowOpen = React.useCallback(
    (key) => () => {
      if (openRowKey.current !== null && key !== openRowKey.current) {
        if (rowRefs.current && rowRefs.current.length) {
          const rowRef = rowRefs.current[openRowKey.current];
          rowRef.closeRow && rowRef.closeRow();
        }
      }
      openRowKey.current = key;
    },
    []
  );

  const goToContactScreen = React.useCallback(() => {
    navigate(SCREENS.ChatContactList);
  }, [navigate]);
  

  const goToChatScreen = React.useCallback(
    (data) => {
      navigate(SCREENS.ChatDetail, {
        conversationId: data.id,
        users: userRef.current,
      });
    },
    [navigate]
  );
  // const onArchiveRoom = (id) => {
  //   const index = unArchives.findIndex((item) => item.id === id);
  //   unArchives[index].archive = true;
  //   dispatch(upArchives([...archives, unArchives[index]]));
  //   dispatch(upUnArchives(lodash.remove(unArchives, (e) => e.id !== id)));
  // };
  // const onDeleteRoom = (id) => {
  //   dispatch(upUnArchives(lodash.remove(unArchives, (e) => e.id !== id)));
  // };
  // const onMuteRoom = (id, value) => {
  //   const index = unArchives.findIndex((item) => item.id === id);
  //   let cUnArchives = [...unArchives];
  //   cUnArchives[index].mute = value;
  //   dispatch(upUnArchives(cUnArchives));
  // };
  // const onPinRoom = (id, value) => {
  //   const index = unArchives.findIndex((item) => item.id === id);
  //   let cUnArchives = [...unArchives];
  //   cUnArchives[index].pin = value;
  //   dispatch(upUnArchives(cUnArchives));
  // };
  // const onUnReadRoom = (id, value) => {
  //   const index = unArchives.findIndex((item) => item.id === id);
  //   let cUnArchives = [...unArchives];
  //   cUnArchives[index].mark_unread = value;
  //   dispatch(upUnArchives(cUnArchives));
  // };
  const onRefresh = () => {
    //if (isLoading) return;
    dispatch(fetchConversations());
  };
  const renderItem = React.useCallback(({ item, index }) => {
    return (
      <ChatHistoryRow
        ref={(el) => {
          return (rowRefs.current[index] = el);
        }}
        index={index}
        key={item.id}
        data={item}
        onRowOpen={onRowOpen}
        action={goToChatScreen}
        // onArchiveAction={onArchiveRoom}
        // onPinAction={onPinRoom}
        // onMuteAction={onMuteRoom}
        // onUnReadAction={onUnReadRoom}
        // onDeleteAction={onDeleteRoom}
      />
    );
  }, []);

  const renderNoHistory = React.useCallback(() => {
    return !isLoading ? (
      <NoHistory />
    ) : (
      <NoHistory label={t("label.loading")} />
    );
  }, []);

  const renderContactIcon = React.useCallback(() => {
    return (
      <TouchableRipple
        onPress={goToContactScreen}
        style={styles.wrapperIconContact}
      >
        <ChatContactIcon
          width={19}
          height={24}
          color={primaryColor[5]}
          style={styles.iconContact}
        />
      </TouchableRipple>
    );
  }, []);

  const onPressArchive = React.useCallback(() => {
    navigate(SCREENS.ChatArchive, { data: archives });
  }, [archives, navigate]);
  const listHeader = () => {
    return (
      <TouchableOpacity onPress={onPressArchive} style={styles.vArchive}>
        <View style={styles.vIconArchive}>
          <IconArchive width={22} height={22} />
        </View>
        <View style={{}}>
          <Text style={styles.tileArchived}>{t("chat.archived_chats")}</Text>
          {archives.length !== 0 && (
            <Text numberOfLines={1} style={styles.nameArchived}>
              {archives.map((a) => a.name).toString()}
            </Text>
          )}
        </View>
      </TouchableOpacity>
    );
  };
  const [refreshing, setRefreshing] = React.useState(false);

  const refreshData = React.useCallback(() => {
    setRefreshing(true);
    //@ts-ignore
    dispatch(refetchContactAndUsers({ callback: () => setRefreshing(false) }));
  }, [dispatch]);

  React.useEffect(() => {
    refreshData();
  }, [refreshData]);

  const onSearch = React.useCallback(
    (keywords) => {
      //setIsLoading(true);
      if (keywords?.length > 0) {
        setListConversation(
          conversations.filter(
            (e) => e?.name?.toUpperCase()?.search(keywords.toUpperCase()) > -1
          )
        );
      } else {
        setListConversation(conversations);
      }
      //setIsLoading(false);
    },
    [conversations, setIsLoading, setListConversation]
  );

  // useFocusEffect(
  //   useCallback(() => {
  //     dispatch(fetchConversations());
  //   }, [])
  // );

  return (
    <SafeAreaView edges={["top", "left", "right"]} style={styles.screen}>
      <Header
        title={t("chat_history.header")}
        componentRight={renderContactIcon()}
        onSearch={onSearch}
      />
      <View style={styles.container}>
        <FlatList
          ListHeaderComponent={listHeader}
          onRefresh={onRefresh}
          refreshing={isLoading}
          style={styles.listHistory}
          data={listConversation}
          renderItem={renderItem}
          // onEndReached={onLoadMore}
          ListEmptyComponent={renderNoHistory}
          keyExtractor={(item, index) =>
            `${item.id}_${index}_${item?.members.length || "-"}`
          }
          showsVerticalScrollIndicator={false}
        />
      </View>
    </SafeAreaView>
  );
};
export default ChatHistory;
