import React from "react";
import { Text, View, FlatList } from "react-native";
import lodash from "lodash";
import { useNavigation } from "@react-navigation/native";
import { useTranslation } from "react-i18next";
import { SafeAreaView } from "react-native-safe-area-context";

import styles from "assets/styles/views/ChatHistory/chatHistoryStyle";
import { default as headerStyles } from "assets/styles/views/ProfilePractice/components/profilePracticeHeaderStyle";

import { SCREENS } from "app/constants";
import { Header } from "react-native-elements";
import ChatHistoryRow from "./components/ChatHistoryRow";
import NoHistory from "components/ChatHistory/NoHistory";
// import { MockData } from "./DummyData";
import BackButton from "components/Button/BackButton";
import StatusBar from "components/App/StatusBar";

import {
  selectArchives,
  selectUnArchives,
  selectRefreshing,
  upArchives,
  upUnArchives,
  deleteArchives,
} from "store/slices/chatSlice";
import { useDispatch, useSelector } from "react-redux";

const ChatArchive = () => {
  const { navigate, goBack } = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const rowRefs = React.useRef([]);
  const openRowKey = React.useRef(null);

  // Selectors
  const isRefreshing = useSelector(selectRefreshing);
  const archives = useSelector(selectArchives);
  const unArchives = useSelector(selectUnArchives);
  const onRowOpen = React.useCallback(
    (key) => () => {
      if (openRowKey.current !== null && key !== openRowKey.current) {
        if (rowRefs.current && rowRefs.current.length) {
          const rowRef = rowRefs?.current[openRowKey.current];
          rowRef?.closeRow && rowRef.closeRow();
        }
      }
      openRowKey.current = key;
    },
    []
  );
  const goToChatScreen = React.useCallback(
    (data) => {
      navigate(SCREENS.ChatDetail, { conversationId: data?.id });
    },
    [navigate]
  );
  const onArchiveRoom = (id) => {
    const index = archives.findIndex((item) => item.id === id);
    archives[index].archive = false;
    dispatch(upUnArchives([...unArchives, archives[index]]));
    dispatch(upArchives(lodash.remove(archives, (e) => e.id !== id)));
  };
  const onDeleteRoom = (id) => {
    dispatch(deleteArchives(id));
  };

  const onMuteRoom = (id, value) => {
    const index = archives.findIndex((item) => item.id === id);
    let cArchives = [...archives];
    cArchives[index].mute = value;
    dispatch(upArchives(cArchives));
  };
  const onPinRoom = (id, value) => {
    const index = archives.findIndex((item) => item.id === id);
    let cArchives = [...archives];
    cArchives[index].pin = value;
    dispatch(upArchives(cArchives));
  };
  const onUnReadRoom = (id, value) => {
    const index = archives.findIndex((item) => item.id === id);
    let cArchives = [...archives];
    cArchives[index].mark_unread = value;
    dispatch(upArchives(cArchives));
  };
  const renderItem = ({ item, index }) => {
    return (
      <ChatHistoryRow
        ref={(el) => {
          return (rowRefs.current[index] = el);
        }}
        index={index}
        isShowLine={index !== 0}
        key={item.id}
        data={item}
        isArchive
        onRowOpen={onRowOpen}
        action={goToChatScreen}
        onArchiveAction={onArchiveRoom}
        onPinAction={onPinRoom}
        onMuteAction={onMuteRoom}
        onUnReadAction={onUnReadRoom}
        onDeleteAction={onDeleteRoom}
      />
    );
  };

  const renderNoHistory = () => {
    return <NoHistory />;
  };
  const onBackPress = React.useCallback(() => {
    goBack();
  }, [goBack]);
  return (
    <View edges={["top", "left", "right"]} style={styles.archive_screen}>
      <StatusBar
        barStyle={"dark-content"}
        backgroundColor={headerStyles.container.backgroundColor}
      />
      <Header
        style={headerStyles.container}
        containerStyle={headerStyles.container}
        centerContainerStyle={headerStyles.centerContainer}
        leftContainerStyle={headerStyles.sideContainer}
        rightContainerStyle={headerStyles.sideContainer}
        backgroundColor={headerStyles.container.backgroundColor}
        leftComponent={
          <BackButton label={t("label.button_back")} onPress={onBackPress} />
        }
        centerComponent={
          <Text style={headerStyles.title}>{t("chat.archived_chats")}</Text>
        }
        rightComponent={<View />}
      />
      <View style={styles.container}>
        <FlatList
          // onRefresh={onRefresh}
          refreshing={isRefreshing}
          style={styles.listHistory}
          data={archives}
          renderItem={renderItem}
          // onEndReached={onLoadMore}
          ListEmptyComponent={renderNoHistory}
          keyExtractor={(item, index) => `${item.id}_${index}`}
          showsVerticalScrollIndicator={false}
        />
      </View>
      <SafeAreaView />
    </View>
  );
};
export default ChatArchive;
