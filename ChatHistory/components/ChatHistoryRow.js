import React, { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { View, TouchableOpacity } from "react-native";
import { Text, Badge } from "react-native-paper";
import { SwipeRow } from "react-native-swipe-list-view";
import { useTranslation } from "react-i18next";
import { formatDateTime } from "app/format";
import { GROUP_INVITATION_STATUS } from "app/constants.ts";

import GroupIcon from "assets/images/svg/IconGroup.svg";

import { showMessageCancelOk } from "components/MyAlert/MyAlert";

import styles from "assets/styles/components/ChatHistory/chatHistoryRowStyle";
import { grayColor, blueColor } from "assets/styles/theme";

import Line from "components/Line/Line";
import Avatar from "components/GeneralAvatar/Avatar.tsx";
import TeTeAvatar from "components/Avatar/TeTeAvatar";
import SwipeableButton from "components/Button/SwipeableButton";

import IconPin from "assets/images/svg/IconPinCustomColor.svg";
import IconPDF from "assets/images/svg/IconPDFsolid.svg";
import IconPhoto from "assets/images/svg/IconPhotoSolid.svg";
import IconVideo from "assets/images/svg/InfoIcon_Video_Chat.svg";
import IconDelete from "assets/images/svg/IconDelete.svg";
import IconMute from "assets/images/svg/IconMute.svg";
import IconArchive from "assets/images/svg/IconArchive.svg";
import IconCheckRead from "assets/images/svg/IconCheckReadBlue.svg";
import IconOneCheck from "assets/images/svg/IconOneCheckBlue.svg";
import IconPinGray from "assets/images/svg/IconPinGray.svg";
import IconMuteGray from "assets/images/svg/IconMuteGray.svg";
import IconScore from "assets/images/svg/IconChatScore.svg";
import IconRecord from "assets/images/svg/IconChatRecord.svg";

import { CHAT_MESSAGE_TYPE, LESSON_TYPE } from "app/constants";

// stores
import { selectProfile } from "store/slices/userSlice";

import {
  // Actions
  readConversation,
  unreadConversation,
  pinConversation,
  unpinConversation,
  muteConversation,
  unmuteConversation,
  archiveConversation,
  unarchiveConversation,
  // Selectors
  selectUserById,
  selectUsers,
  selectListGroups,
} from "store/slices/chatSlice";

const ChatHistoryRow = React.forwardRef((props, ref) => {
  const contacts = useSelector((state) => state.chat.contacts);
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const {
    data,
    isShowLine = true,
    onRowOpen,
    index,
    action,
    isArchive = false,
    onDeleteAction,
  } = props;

  const {
    id,
    name,
    avatar,
    type,
    pin,
    mark_unread,
    unread,
    seen,
    archive,
    mute,
    updated_at,
    members = [],
    source,
  } = data;

  const lastChat = data.last_chat;
  const currentRow = React.useRef(null);
  const profile = useSelector(selectProfile);
  const groups = useSelector(selectListGroups);
  const group = groups?.find((i) => i?.id === source);
  const usersChat = useSelector(selectUsers);
  const isLastOne = profile.id === lastChat?.sender_id;

  const [nickNameChat, setNickNameChat] = React.useState();
  React.useEffect(() => {
    const userChat = usersChat.find((item) => item?.id === data?.name)
    if (userChat?.first_name === "" && userChat?.last_name === "") {
      setNickNameChat(userChat?.nickname);
    }
  }, [nickNameChat, usersChat, data?.name])

  const notInContact = useEffect(() => {
    let results = false;
    if (data.type !== 1) {
      members.forEach((item) => {
        if (profile?.id !== item && item !== "__tete") {
          const user = usersChat.find((i) => i?.id === item);
          if (user) {
            results = !user.contact_added;
          }
        }
      });
    }
    return results;
  }, [data.type, members, profile?.id, usersChat, contacts]);
  const isTete = id === `__tete_${profile.id}`;
  const lastUser = useSelector((state) =>
    selectUserById(state, lastChat?.sender_id)
  );
  let lastSenderName = null;
  if (isLastOne) lastSenderName = t("chat_history.you");
  else {
    if (lastChat) {
      // If group
      if (type !== 0) {
        if (lastUser && lastUser.first_name) {
          lastSenderName = lastUser.first_name;
          if (lastUser.last_name) lastSenderName += " " + lastUser.last_name;
        }
      }
    }
  }

  React.useImperativeHandle(ref, () => ({
    closeRow: () => {
      if (currentRow.current) {
        currentRow.current.closeRow();
      }
    },
  }));

  //-----------------------------HANDLE---------------------------------

  const onPressChatHistoryRow = React.useCallback(() => {
    action && action(data, index);
  }, [action, data, index]);

  const onArchiveRoomHandle = React.useCallback(() => {
    if (!id) return;
    if (archive) dispatch(unarchiveConversation({ conversationId: id }));
    else dispatch(archiveConversation({ conversationId: id }));
  }, [dispatch, id, archive]);

  const onDeleteRoomHandle = React.useCallback(() => {
    if (data && data.id) {
      showMessageCancelOk(
        t("chat.conversation.chat_delete_confirm", { name: data.name }),
        null,
        () =>
          onDeleteAction &&
          onDeleteAction(data.id, data.archive === false ? true : false)
      );
    }
  }, [data, t, onDeleteAction]);

  const onPinRoomHandle = React.useCallback(() => {
    if (!id) return;
    currentRow.current.closeRow();
    if (pin) dispatch(unpinConversation({ conversationId: id }));
    else dispatch(pinConversation({ conversationId: id }));
  }, [dispatch, id, pin]);

  const onMuteRoomHandle = React.useCallback(() => {
    if (!id) return;
    currentRow.current.closeRow();
    if (mute) dispatch(unmuteConversation({ conversationId: id }));
    else dispatch(muteConversation({ conversationId: id }));
  }, [dispatch, id, mute]);

  const onUnReadRoomHandle = React.useCallback(() => {
    if (!id) return;
    currentRow.current.closeRow();
    if (mark_unread) dispatch(readConversation({ conversationId: id }));
    else dispatch(unreadConversation({ conversationId: id }));
  }, [dispatch, id, mark_unread]);

  //------------------------------------------------------------------------
  const renderItemStatus = React.useCallback(() => {
    if (pin) {
      return <IconPinGray />;
    }
    if (mark_unread) {
      return <Badge style={styles.unreadMarker} />;
    }

    return null;
  }, [pin, mark_unread]);

  const renderBadge = React.useCallback(() => {
    if (isLastOne) {
      return seen ? (
        <IconCheckRead style={styles.marginRight5} />
      ) : (
        <IconOneCheck style={styles.marginRight5} />
      );
    } else {
      if (unread) {
        return (
          <Badge style={styles.unreadBadge} size={20}>
            {unread}
          </Badge>
        );
      }
    }

    return null;
  }, [isLastOne, unread, seen]);
  const marginMute = { marginRight: data?.mute ? 35 : 0 };
  const displayLastMessage = React.useCallback(() => {
    let textMessage = "";
    let Icon = null;
    let lastMessageStyle = {};
    if (notInContact) {
      textMessage = t("chat_history.not_in_contact");
      lastMessageStyle = styles.lastMessageAction;
    } else {
      if (lastChat) {
        const message = lastChat?.message;

        const chatType = lastChat.type;
        switch (chatType) {
          case CHAT_MESSAGE_TYPE.Text:
            textMessage = message;

            break;
          case CHAT_MESSAGE_TYPE.Score:
          case CHAT_MESSAGE_TYPE.UserScore:
          case CHAT_MESSAGE_TYPE.Record:
            textMessage = "  " + JSON.parse(message).name;
            break;
          case CHAT_MESSAGE_TYPE.Video:
            console.log("JSON.parse(message).title",JSON.parse(message)?.content?.en?.title)
            textMessage = JSON.parse(message)?.content?.en?.title
            break;
          case CHAT_MESSAGE_TYPE.Image:
          case CHAT_MESSAGE_TYPE.Audio:
          case CHAT_MESSAGE_TYPE.File:
            const messageData = JSON.parse(message);
            var filename = messageData.url.substr(
              messageData.url.lastIndexOf("/") + 1
            );
            textMessage = filename;
            break;
          case CHAT_MESSAGE_TYPE.Lesson:
            const LessonType = JSON.parse(message).type;
            if (LessonType === LESSON_TYPE.Score) {
              textMessage = "  " + JSON.parse(message).name;
            } else if (LessonType === LESSON_TYPE.File) {
              const messageDataLesson = JSON.parse(message);
              var filename = messageDataLesson?.file.substr(
                messageDataLesson?.file.lastIndexOf("/") + 1
              );
              textMessage = filename;
            }

            break;
          case CHAT_MESSAGE_TYPE.LessonPlan:
            textMessage = "  " + JSON.parse(message).name;
            break;

          case CHAT_MESSAGE_TYPE.Deleted:
            textMessage = t("chat.attach.removed");
            break;

          default:
            textMessage = message;
            break;
        }
        switch (chatType) {
          case CHAT_MESSAGE_TYPE.LessonPlan:
          case CHAT_MESSAGE_TYPE.Score:
            Icon = <IconScore style={styles.marginRight5} />;
            break;
          case CHAT_MESSAGE_TYPE.Record:
            Icon = <IconRecord style={styles.marginRight5} />;
            break;
          case CHAT_MESSAGE_TYPE.Image:
            Icon = <IconPhoto style={styles.marginRight5} />;
            break;
          case CHAT_MESSAGE_TYPE.Video:
            Icon = <IconVideo style={styles.marginRight5} />;
            break;
          case CHAT_MESSAGE_TYPE.File:
            const text = JSON.parse(lastChat.message);
            const arrElementUrl = text?.url.split("/");
            const title = arrElementUrl[arrElementUrl.length - 1];
            const isPDF = title.includes("pdf");
            if (isPDF) {
              Icon = <IconPDF style={styles.marginRight5} />;
            } else {
              Icon = <IconPhoto style={styles.marginRight5} />;
            }
            break;
          case CHAT_MESSAGE_TYPE.Lesson:
            const LessonType = JSON.parse(message).type;
            if (LessonType === LESSON_TYPE.Score) {
              Icon = <IconScore style={styles.marginRight5} />;
            } else if (LessonType === LESSON_TYPE.File) {
              const text = JSON.parse(message);
              const arrElementUrl = text?.file.split("/");
              const title = arrElementUrl[arrElementUrl.length - 1];
              const isPDF = title.includes("pdf");
              if (isPDF) {
                Icon = <IconPDF style={styles.marginRight5} />;
              } else {
                Icon = <IconPhoto style={styles.marginRight5} />;
              }
            }

            break;
          default:
            Icon = null;
            break;
        }
        lastMessageStyle = styles.lastMessage;
      } else {
        // If group
        if (type === 0) {
          textMessage = t("chat_history.new_friend", { name });
        } else {
          textMessage = t("chat_history.new_group");
        }
        lastMessageStyle = styles.lastMessageAction;
      }

      if (group?.invite_status === GROUP_INVITATION_STATUS.Pending) {
        textMessage = t("chat_history.invite_you_to_group", {
          name: (group?.invited_by?.name || "Noname") + " ",
        });
        lastMessageStyle = styles.lastMessageAction;
      }
    }
    return (
      <View style={styles.vChatMessage}>
        {Icon}
        <Text style={lastMessageStyle} numberOfLines={1}>
          {textMessage}
        </Text>
      </View>
    );
  }, [notInContact, t, lastChat, type, name, group]);

  return (
    <SwipeRow
      // {...props}
      ref={currentRow}
      leftOpenValue={isArchive ? 0 : 70}
      rightOpenValue={-140}
      onRowOpen={onRowOpen(index)}
    >
      <View style={styles.hideView}>
        <View style={styles.leftSectionHiddenView}>
          {!isArchive && (
            <>
              {/* <SwipeableButton
                title={
                  mark_unread
                    ? t("chat_history.read")
                    : t("chat_history.unread")
                }
                onPress={onUnReadRoomHandle}
                color="#197aff"
                isLeft={true}
                icon={<IconRead size={24} />}
              /> */}
              <SwipeableButton
                title={pin ? t("chat_history.unpin") : t("chat_history.pin")}
                onPress={onPinRoomHandle}
                color={grayColor[4]}
                isLeft={true}
                icon={<IconPin color={blueColor[0]} />}
              />
            </>
          )}
        </View>
        <View style={styles.rightSectionHiddenView}>
          <SwipeableButton
            title={
              archive ? t("chat_history.unarchive") : t("chat_history.archive")
            }
            onPress={onArchiveRoomHandle}
            color={grayColor[4]}
            icon={<IconArchive />}
          />
          <View style={styles.verticalLine} />
          {isArchive && (
            <SwipeableButton
              title={t("chat_history.delete")}
              onPress={onDeleteRoomHandle}
              color={grayColor[4]}
              icon={<IconDelete />}
            />
          )}
          {!isArchive && (
            <SwipeableButton
              title={mute ? t("chat_history.unmute") : t("chat_history.mute")}
              onPress={onMuteRoomHandle}
              color={grayColor[4]}
              icon={<IconMute />}
            />
          )}
        </View>
      </View>

      <TouchableOpacity
        activeOpacity={1}
        style={[styles.item, pin && styles.isPinWrapper]}
        onPress={onPressChatHistoryRow}
      >
        <View style={styles.leftItem}>
          {isTete ? (
            <TeTeAvatar size={40} />
          ) : (
            <Avatar image={avatar} text={nickNameChat ? (nickNameChat) : (name)} size={40} />
          )}
        </View>

        <View style={styles.rightItem}>
          {isShowLine && <Line />}
          <View style={styles.rightContainerWrapper}>
            <View style={styles.rightInfoItemWrapper}>
              <View style={styles.historyInfoWrapper}>
                <View style={styles.nameWrapper}>
                  {type === 1 && (
                    <GroupIcon
                      width={16}
                      height={15}
                      style={styles.groupIcon}
                    />
                  )}
                  <View style={[styles.vRow, marginMute]}>
                    <Text numberOfLines={1} style={styles.historyName}>
                      {nickNameChat ? (nickNameChat) : (name) || ""}
                    </Text>
                    {mute ? <IconMuteGray size={5} /> : null}
                  </View>
                </View>
              </View>
              <Text style={styles.textDateTime}>
                {formatDateTime(updated_at, i18n.language)}
              </Text>
            </View>
            <View style={styles.wrapperMessage}>
              <View style={styles.lastMessageWrapper}>
                {lastSenderName && !notInContact && (
                  <Text numberOfLines={1} style={[styles.userName]}>
                    {group?.invite_status !== GROUP_INVITATION_STATUS.Pending
                      ? lastSenderName + ":"
                      : null}
                  </Text>
                )}
                {displayLastMessage()}
              </View>
              {renderBadge()}
              {renderItemStatus()}
            </View>
          </View>
        </View>

        {/* <View style={styles.vPin}>
          <IconPin  width={5} height={5} />
        </View> */}
      </TouchableOpacity>
    </SwipeRow>
  );
});

export default ChatHistoryRow;
