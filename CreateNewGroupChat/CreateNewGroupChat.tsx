import React, { useCallback } from "react";
import {
  View,
  Text,
  SectionList,
  RefreshControl,
  SafeAreaView,
  TextInput,
} from "react-native";
// import { Header } from "react-native-elements";
import Header from "components/App/NormalHeader";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import lodash from "lodash";
import { useTranslation } from "react-i18next";

import styles from "assets/styles/views/CreateNewGroupChat/createNewGroupChatStyle";
import { default as headerStyles } from "assets/styles/views/ProfilePractice/components/profilePracticeHeaderStyle";

import ContactRow from "components/ChatContact/ContactRow";
import NoContact from "components/ChatContact/NoContact";
import StatusBar from "components/App/StatusBar";

import {
  // Actions
  addNewGroups,
  listContacts,
  // Selectors
  selectGroupChatData,
  cleanGroupData,
  selectLoading,
  selectContacts,
} from "store/slices/chatSlice";

import { useDispatch, useSelector } from "react-redux";
import { grayColor } from "assets/styles/theme";
import { FileType } from "types/File";
import { ChatPerson } from "app/chatUtils";
import ButtonText from "components/Button/ButtonText";
import { AvatarEditForChat } from "components/ChooseImage/AvatarEditForChat";

const CreateNewGroupChat = () => {
  const navigation = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const contacts = useSelector(selectContacts);
  const groupChatData = useSelector(selectGroupChatData);
  const isLoading = useSelector(selectLoading);

  const [contact, setContact] = React.useState<any[]>([]);
  const [refreshing, setRefreshing] = React.useState(false);
  const [selectedContacts, setSelectedContacts] = React.useState<ChatPerson[]>(
    []
  );
  const [groupName, setGroupName] = React.useState("");
  const dataSelectedImage = React.useRef<FileType | null>(null);
  const isCreateSuccess = groupChatData && !lodash.isNil(groupChatData.id);

  React.useEffect(() => {
    const map: any = {};
    contacts.forEach((u: any) => {
      u.name = u.name || u.id || "";
      let c0 = u.name.charAt(0).toUpperCase();
      if (!/[A-Z]/.test(c0)) {
        c0 = "#";
      }
      if (!map[c0]) {
        map[c0] = [];
      }
      map[c0].push(u);
    });

    let groups: any[] = Object.keys(map).map((k) => ({
      key: k,
      data: map[k],
    }));
    groups = lodash.orderBy(groups, "key");
    groups.forEach((gr) => {
      gr.data = lodash.orderBy(gr.data, "name");
    });
    setContact(groups);
  }, [contacts]);

  React.useEffect(() => {
    if (isCreateSuccess) {
      const timeout = setTimeout(() => {
        //@ts-ignore
        dispatch(cleanGroupData());
        //@ts-ignore
        navigation.pop(2);
      }, 1200);
      return () => clearTimeout(timeout);
    }
  }, [dispatch, isCreateSuccess, navigation]);

  const selectedImage = ({ uri }: { uri: string }) => {
    dataSelectedImage.current = {
      uri: uri,
      type: "image/jpeg",
      name: `${new Date().getTime()}.jpg`,
    };
  };

  //------------------------------------------------------------------------

  //-----------------------------FETCH AND LOAD MORE------------------------
  const refreshData = React.useCallback(() => {
    setRefreshing(true);
    //@ts-ignore
    dispatch(listContacts({ callback: () => setRefreshing(false) }));
  }, [dispatch]);

  const onLoadMore = React.useCallback(() => {}, []);

  //------------------------------------------------------------------------

  //----------------------GET DISABLE RIGHT BUTTON-------------------------
  const getIsDisableRightButton = () => {
    if (isLoading || isCreateSuccess) return true;
    let error = false;
    if (!lodash.isArray(selectedContacts) || selectedContacts.length === 0) {
      error = true;
    }
    if (lodash.isEmpty(groupName.trim())) {
      error = true;
    }
    return error;
  };

  //-----------------------------------------------------------------------

  //-------------------------CREATE GROUP--------------------------------
  const onCreateGroup = React.useCallback(async () => {
    let listMemberIds: any[] = [];
    selectedContacts.forEach((item: ChatPerson) => {
      listMemberIds.push(item?.id);
    });
    const data: {
      group: {
        name: string;
        avatar?: FileType;
      };
      members: any;
    } = {
      group: { name: groupName.trim() },
      members: { user_ids: listMemberIds },
    };
    if (dataSelectedImage.current) {
      data.group.avatar = dataSelectedImage.current;
    }
    dispatch(
      //@ts-ignore
      addNewGroups(data)
    );
  }, [selectedContacts, groupName, dispatch]);

  const onSelectContact = React.useCallback(
    (item: ChatPerson) => {
      if (lodash.isArray(selectedContacts)) {
        if (selectedContacts.length === 0) {
          selectedContacts.push(item);
          setSelectedContacts([...selectedContacts]);
        } else {
          const index = selectedContacts.findIndex(
            (selectedContact) => selectedContact.id === item.id
          );
          if (index === -1) {
            selectedContacts.push(item);
          } else {
            selectedContacts.splice(index, 1);
          }
          setSelectedContacts([...selectedContacts]);
        }
      }
    },
    [selectedContacts]
  );

  const onChangeGroupName = React.useCallback((text) => {
    setGroupName(text);
  }, []);
  //-------------------------------------------------------------------

  useFocusEffect(
    useCallback(() => {
      setGroupName("");
      setRefreshing(false);
      setSelectedContacts([]);
    }, [])
  );

  //-----------------------------RENDER VIEW---------------------------------
  const renderItem = (props: any) => {
    const { item, section, index } = props;
    let isChecked = false;
    if (lodash.isArray(selectedContacts) && selectedContacts.length > 0) {
      const index2 = selectedContacts.findIndex((d) => d.id === item.id);
      if (index2 !== -1) {
        isChecked = true;
      }
    }
    return (
      <ContactRow
        key={item.id}
        action={onSelectContact}
        isChecked={isChecked}
        section={section}
        index={index}
        item={item}
        type={"newGroup"}
      />
    );
  };

  const renderListHeader = () => {
    return (
      <View style={styles.firstSection}>
        <AvatarEditForChat setSelectImage={selectedImage} />
        <View style={styles.rightSectionWrapper}>
          <TextInput
            editable
            placeholder={t("chat_contact.group_name")}
            placeholderTextColor={grayColor[1]}
            onChangeText={onChangeGroupName}
            value={groupName}
            maxLength={50}
            style={styles.inputName}
          />
        </View>
      </View>
    );
  };

  const renderNoHistory = () => {
    return <NoContact />;
  };

  const getTitleRightButton = () => {
    let txtRightButton = t("general.save");

    if (isLoading) {
      txtRightButton = t("general.saving") + "...";
    }
    if (isCreateSuccess) {
      txtRightButton = t("general.saved");
    }

    return txtRightButton;
  };

  const isDisable = getIsDisableRightButton();

  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={"dark-content"}
        backgroundColor={headerStyles.container.backgroundColor}
      />
      <Header
        navigation={navigation}
        title={t("chat_contact.new_group")}
        style={styles.sectionContainer}
        rightComponent={() => (
          <ButtonText
            isDisableAction={isDisable}
            action={onCreateGroup}
            title={getTitleRightButton()}
            styleText={styles.txtRightButton}
          />
        )}
      />
      {renderListHeader()}
      <SectionList
        contentContainerStyle={styles.sectionContainer}
        showsVerticalScrollIndicator={false}
        refreshing
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={refreshData} />
        }
        style={{}}
        sections={contact}
        renderSectionHeader={({ section: { key } }) => (
          <Text style={styles.sectionHeader}>{key}</Text>
        )}
        renderItem={renderItem}
        ListEmptyComponent={renderNoHistory()}
        onEndReached={onLoadMore}
        keyExtractor={(item, index) => `${item.id}_${index}`}
        extraData={contact}
      />
      <SafeAreaView />
    </View>
  );
};

export default CreateNewGroupChat;
