import { useNavigation } from "@react-navigation/native";
import { ChatPerson } from "app/chatUtils";
import { SCREENS } from "app/constants";
import { makeFullname } from "app/helper";
import styles from "assets/styles/views/ChatManagerGroup/component/MemberRow";
import Avatar from "components/GeneralAvatar/Avatar";
import Line from "components/Line/Line";
import { showMessageCancelOk } from "components/MyAlert/MyAlert";
import LastSeenText from "components/Text/LastSeenText";
import React from "react";
import { useTranslation } from "react-i18next";
import { Text, View } from "react-native";
import { useSelector } from "react-redux";
// stores
import { selectLastOnlineById } from "store/slices/chatSlice";

interface Props {
  data: ChatPerson[];
  item: ChatPerson;
  index: number;
  onRowRemove: () => void;
  role?: "admin" | "mod" | "member";
}
const MemberRow = (props: Props) => {
  const { t } = useTranslation();
  const { data, item, index, onRowRemove, role = "member" } = props;
  const navigation = useNavigation();
  const lastOnline = useSelector((state) =>
    // @ts-ignore
    selectLastOnlineById(state, item.id)
  );
  const fullname = makeFullname(item?.first_name!, item?.last_name!);
  let isShowLine = true;
  if (data?.length) {
    isShowLine = index + 1 !== data.length;
  }

  //-----------------------------HANDLE---------------------------------

  const onRemoveMember = React.useCallback(() => {
    if (item && item.id) {
      showMessageCancelOk(t("group_detail.confirm_remove_member"), null, () => {
        onRowRemove();
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [item, t]);

  const onAvatarPress = React.useCallback(() => {
    if (item?.id)
      //@ts-ignore
      navigation.navigate(SCREENS.ChatContactDetail, { userId: item.id });
  }, [navigation, item]);
  //------------------------------------------------------------------------

  return (
    <View style={styles.item}>
      <Avatar
        hidden={false}
        text={fullname || ""}
        image={item?.avatar || ""}
        style={styles.icon}
        size={35}
        onPress={onAvatarPress}
      />
      <View style={styles.rightItem}>
        <View style={styles.rightWrapper}>
          <View style={styles.info}>
            <Text numberOfLines={1} style={styles.contactName}>
              {fullname}
            </Text>
            <LastSeenText lastSeen={lastOnline?.lastSeen} />
          </View>
          {role !== "member" && (
            <>
              <Text onPress={onRemoveMember} style={styles.txtRemove}>
                {t("general.remove")}
              </Text>
              <View style={[styles.wrapperRightButton, { opacity: 0.3 }]}>
                <Text style={styles.txtPromote}>Promote</Text>
                {/* <Text
              style={styles.txtMod}
            >
              Mod
            </Text> */}
              </View>
            </>
          )}
        </View>
        {isShowLine && <Line style={{}} />}
      </View>
    </View>
    // </SwipeRow>
  );
};

export default MemberRow;
