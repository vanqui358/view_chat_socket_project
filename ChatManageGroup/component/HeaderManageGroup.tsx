import React, { useEffect, useMemo, useState } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";

import { useTranslation } from "react-i18next";
import { Header } from "react-native-elements";
import { TouchableRipple } from "react-native-paper";
import { NavigationProp } from "@react-navigation/native";
import { SCREENS } from "app/constants";

// components
import StatusBar from "components/App/StatusBar";
import BackButton from "components/Button/BackButton";
import Avatar from "components/GeneralAvatar/Avatar";

import styles from "assets/styles/views/ChatManagerGroup/component/HeaderManageGroup";
import Addmember from "assets/images/svg/IconNewContact.svg";
import MuteNotification from "assets/images/svg/MuteNotification.svg";
import UnMuteNotification from "assets/images/svg/UnMuteNotification.svg";
import IconLeaveGroup from "assets/images/svg/IconLeaveGroup.svg";
import { showMessageCancelOk } from "components/MyAlert/MyAlert";

import { useDispatch, useSelector } from "react-redux";
// stores
import {
  // Actions
  leaveGroup,
  muteConversation,
  selectConversations,
  //
  selectDataGroupDetail,
  unmuteConversation,
} from "store/slices/chatSlice";
import Loading from "components/Loading/Loading";
import { selectProfile } from "store/slices/userSlice";

type ScreenProps = {
  data: { name: string; id: string; members: any };
  navigation: NavigationProp<any>;
  conversationId: string;
  editGroupModal: React.MutableRefObject<any>;
};
export default function HeaderManageGroup(props: ScreenProps) {
  const { t } = useTranslation();
  const { navigation, data, conversationId, editGroupModal } = props;
  const dispatch = useDispatch();
  //@ts-ignore
  const isLoading = useSelector((state) => state.chat.loading);
  const conversations = useSelector(selectConversations);
  const groupInfo = useSelector(selectDataGroupDetail);
  const profile = useSelector(selectProfile);
  const bgColor = styles.container.backgroundColor;
  //@ts-ignore
  const conversation = conversations.find((item) => item.id === conversationId);

  const myRole = useMemo(() => {
    if (!groupInfo.members) return 0;
    return (
      groupInfo?.members?.find((item: any) => item?.id === profile?.id)?.role ||
      0
    );
  }, [groupInfo.members, profile.id]);

  const onBackPress = React.useCallback(() => {
    navigation.goBack();
  }, [navigation]);
  const onPressAddMember = React.useCallback(() => {
    navigation.navigate(SCREENS.ChatGroupAddMember);
  }, [navigation]);
  const onPressMute = React.useCallback(() => {
    if (conversationId) {
      if (conversation?.mute) {
        //@ts-ignore
        dispatch(unmuteConversation({ conversationId }));
      } else {
        //@ts-ignore
        dispatch(muteConversation({ conversationId }));
      }
    }
  }, [conversation?.mute, conversationId, dispatch]);

  const onLeaveGroup = React.useCallback(() => {
    //@ts-ignore
    dispatch(leaveGroup(data.id));
    navigation.navigate("ChatTab");
  }, [data.id, dispatch, navigation]);
  const onPressLeaveGroup = React.useCallback(() => {
    if (data && data.id) {
      showMessageCancelOk(
        t("group_detail.confirm_leave_group"),
        null,
        onLeaveGroup
      );
    }
  }, [data, onLeaveGroup, t]);

  const centerComponent = () => {
    return (
      <View style={styles.vCenter}>
        <Avatar size={120} image={groupInfo?.avatar} text={data?.name} />
        <Text style={styles.nameGroup}>{data.name}</Text>
        <Text style={styles.textMembers}>
          {t("group_detail.number_members", {
            count: data.members?.length || 1,
          })}
        </Text>
      </View>
    );
  };
  const onPressEdit = React.useCallback(() => {
    if (editGroupModal.current) {
      editGroupModal.current.show();
    }
  }, [editGroupModal]);

  const IconMute = conversation?.mute ? UnMuteNotification : MuteNotification;
  return (
    <>
      <StatusBar barStyle={"dark-content"} backgroundColor={bgColor} />
      <Header
        style={styles.container}
        containerStyle={styles.container}
        centerContainerStyle={styles.centerContainer}
        leftContainerStyle={styles.sideContainer}
        rightContainerStyle={styles.sideContainer}
        backgroundColor={bgColor}
        leftComponent={
          <BackButton label={t("general.back")} onPress={onBackPress} />
        }
        centerComponent={centerComponent()}
        rightComponent={
          myRole !== 0 ? (
            <TouchableRipple onPress={onPressEdit}>
              <Text style={styles.text}>{t("general.edit")}</Text>
            </TouchableRipple>
          ) : (
            <></>
          )
        }
      />
      <View style={styles.vAction}>
        <TouchableOpacity onPress={onPressAddMember} style={styles.vItemAction}>
          <View style={styles.wrapperIcon}>
            <Addmember height={30} />
          </View>
          <Text style={styles.textItemAction}>
            {t("group_detail.add_members")}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onPressMute} style={styles.vItemAction}>
          <View style={styles.wrapperIcon}>
            <IconMute height={30} />
          </View>
          <Text style={styles.textItemAction}>
            {conversation?.mute
              ? t("chat_history.unmute")
              : t("group_detail.mute")}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={onPressLeaveGroup}
          style={styles.vItemAction}
        >
          <View style={styles.wrapperIcon}>
            <IconLeaveGroup height={30} />
          </View>
          <Text style={styles.textItemAction}>{t("group_detail.leave")}</Text>
        </TouchableOpacity>
      </View>
    </>
  );
}
