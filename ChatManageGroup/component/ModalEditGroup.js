import React from "react";
import { View, Text, StyleSheet, Keyboard } from "react-native";
import lodash from "lodash";
import { useTranslation } from "react-i18next";

import Line from "components/Line/Line";
import ButtonText from "components/Button/ButtonText";
import styles from "assets/styles/components/MyPopup/popupAddNewContactStyle";
import Loading from "components/Loading/Loading";
import AvatarAddImage from "components/ChooseImage/AvatarAddImage";
import PopupBase from "components/MyPopup/base/PopupBase";
import TextInput, {
  Status as TextInputStatus,
} from "components/Input/TextInput";

// stores
import {
  // Actions
  editGroup,
  cleanGroupData,
  groupDetail,
  // Selectors
  selectLoading,
  selectDataGroupDetail,
  selectGroupChatData,
} from "store/slices/chatSlice";
import { useDispatch, useSelector } from "react-redux";
import { AvatarEditForChat } from "components/ChooseImage/AvatarEditForChat";
import Animated, {
  useAnimatedStyle,
  withTiming,
} from "react-native-reanimated";

const ModalEditGroup = React.forwardRef(({ callback }, ref) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const dataSelectedImage = React.useRef(null);
  const dataGroupDetail = useSelector(selectDataGroupDetail);
  const groupRequest = useSelector(selectGroupChatData);
  const [isShowPopup, setIsShowPopup] = React.useState(false);
  const [groupName, setGroupName] = React.useState(dataGroupDetail.name);
  const [groupNameError, setGroupNameError] = React.useState(null);

  const loading = useSelector(selectLoading);

  const groupInfo = React.useMemo(
    () => ({
      name: groupName?.trim(),
      groupId: dataGroupDetail.id,
    }),
    [groupName, dataGroupDetail.id]
  );

  React.useImperativeHandle(ref, () => ({
    show: () => {
      showPopup();
    },
  }));

  React.useEffect(() => {
    setGroupName(dataGroupDetail.name || "");
  }, [dataGroupDetail.name]);

  React.useEffect(() => {
    if (groupRequest && !lodash.isNil(groupRequest.id)) {
      dispatch(cleanGroupData());
      dispatch(groupDetail(dataGroupDetail.id));
    }
  }, [dataGroupDetail.id, dispatch, groupRequest, hidePopup]);

  const onEditGroup = React.useCallback(async () => {
    if (lodash.isEmpty(groupInfo.name)) {
      setGroupNameError(t("general.helptext_field_required"));
      return;
    }
    Keyboard.dismiss();
    setGroupNameError(null);
    if (dataSelectedImage.current) {
      groupInfo.avatar = dataSelectedImage.current;
    }
    dispatch(editGroup(groupInfo));
    hidePopup();
  }, [groupInfo, dispatch, hidePopup, t]);

  const resetFields = React.useCallback(() => {
    setGroupNameError(null);
    dataSelectedImage.current = null;
  }, []);

  const hidePopup = React.useCallback(() => {
    resetFields();
    setGroupNameError(null);
    setIsShowPopup(false);
  }, [resetFields]);

  const showPopup = React.useCallback(() => {
    setIsShowPopup(true);
  }, []);

  const onChangeGroupName = React.useCallback((text) => {
    setGroupName(text);
  }, []);

  const selectedImage = ({ uri }) => {
    dataSelectedImage.current = {
      uri,
      type: "image/jpeg",
      name: `${new Date().getTime()}.jpg`,
    };
  };

  const fakeModalAniStyle = useAnimatedStyle(() => {
    return {
      opacity: isShowPopup ? withTiming(1) : withTiming(0),
    };
  });

  return (
    <Animated.View
      style={[
        StyleSheet.absoluteFillObject,
        styles.fakeModal,
        fakeModalAniStyle,
      ]}
      pointerEvents={isShowPopup ? "auto" : "none"}
    >
      <View style={styles.container}>
        <View style={styles.header}>
          <ButtonText
            title={t("general.cancel")}
            titleColor="#197aff"
            action={hidePopup}
          />
          <Text style={styles.headerTitle}>{t("edit_group.header")}</Text>
          <ButtonText
            titleColor="#197aff"
            title={t("general.save")}
            action={onEditGroup}
          />
        </View>
        <Line />
        <View style={styles.body}>
          <View style={styles.firstSection}>
            <AvatarEditForChat setSelectImage={selectedImage} />
            <View style={styles.rightSectionWrapper}>
              <TextInput
                label={
                  t("chat_contact.group_name") + " " + t("general.required")
                }
                onChangeText={onChangeGroupName}
                value={groupName}
                maxLength={50}
                helpText={groupNameError}
                status={
                  groupNameError ? TextInputStatus.Error : TextInputStatus.Info
                }
              />
            </View>
          </View>
        </View>
        {/* <Loading isLoading={loading} /> */}
      </View>
    </Animated.View>
  );
});
export default ModalEditGroup;
