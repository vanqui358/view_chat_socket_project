import React from "react";
import {
  View,
  Text,
  SectionList,
  RefreshControl,
  SafeAreaView,
} from "react-native";
import { Header } from "react-native-elements";
import { TouchableRipple } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";
import lodash from "lodash";
import { useTranslation } from "react-i18next";

import styles from "assets/styles/views/CreateNewGroupChat/createNewGroupChatStyle";
import { default as headerStyles } from "assets/styles/views/ProfilePractice/components/profilePracticeHeaderStyle";

import ContactRow from "components/ChatContact/ContactRow";
import NoContact from "components/ChatContact/NoContact";
import { showMessage } from "components/MyAlert/MyAlert";
import StatusBar from "components/App/StatusBar";

import {
  // Actions
  addMembers,
  groupDetail,
  listContacts,
  // Selectors
  selectContactsAdded,
  selectAddMemberStatus,
  setAddMemberStatus,
  selectDataGroupDetail,
} from "store/slices/chatSlice";
import { useDispatch, useSelector } from "react-redux";

const AddMember = () => {
  const navigation = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const contacts = useSelector(selectContactsAdded);
  const addMemberStatus = useSelector(selectAddMemberStatus);
  const dataGroupDetail = useSelector(selectDataGroupDetail);
  const [contact, setContact] = React.useState([]);
  const [indicator, setIndicator] = React.useState({
    isLoading: false,
    isLoadingRefreshControl: false,
  });
  const [selectedContacts, setSelectedContacts] = React.useState([]);
  React.useEffect(() => {
    dispatch(listContacts());
  }, [dispatch]);
  React.useEffect(() => {
    const map = {};
    const newContacts = lodash.differenceBy(
      contacts,
      dataGroupDetail.members,
      "id"
    );
    newContacts.forEach((u) => {
      u.name = u.name || u.id || "";
      let c0 = u.name.charAt(0).toUpperCase();
      if (!/[A-Z]/.test(c0)) {
        c0 = "#";
      }
      if (!map[c0]) {
        map[c0] = [];
      }
      map[c0].push(u);
    });

    let groups = Object.keys(map).map((k) => ({
      key: k,
      data: map[k],
    }));
    groups = lodash.orderBy(groups, "key");
    groups.forEach((gr) => {
      gr.data = lodash.orderBy(gr.data, "name");
    });
    setContact(groups);
  }, [contacts, dataGroupDetail.members]);
  React.useEffect(() => {
    if (addMemberStatus) {
      dispatch(setAddMemberStatus());
      dispatch(groupDetail(dataGroupDetail.id));
      navigation.goBack(null);
    }
  }, [addMemberStatus, dataGroupDetail.id, dispatch, navigation]);

  const refreshData = React.useCallback(() => {
    dispatch(listContacts());
  }, [dispatch]);

  const onLoadMore = React.useCallback(() => {}, []);

  const onAddMember = React.useCallback(async () => {
    if (!lodash.isArray(selectedContacts) || selectedContacts.length === 0) {
      showMessage("Please select contact before add.");
      return;
    }
    let listMemberIds = [];
    selectedContacts.forEach((item) => {
      listMemberIds.push(item.id);
    });
    const data = {
      members: { user_ids: listMemberIds },
      groupId: dataGroupDetail.id,
    };
    dispatch(addMembers(data));
  }, [selectedContacts, dataGroupDetail.id, dispatch]);

  const onSelectContact = React.useCallback(
    (item) => {
      if (lodash.isArray(selectedContacts)) {
        if (selectedContacts.length === 0) {
          selectedContacts.push(item);
          setSelectedContacts([...selectedContacts]);
        } else {
          const index = selectedContacts.findIndex(
            (selectedContact) => selectedContact.id === item.id
          );
          if (index === -1) {
            selectedContacts.push(item);
          } else {
            selectedContacts.splice(index, 1);
          }
          setSelectedContacts([...selectedContacts]);
        }
      }
    },
    [selectedContacts]
  );
  //-------------------------------------------------------------------

  //-----------------------------RENDER VIEW---------------------------------
  const renderItem = (props) => {
    const { item, section, index } = props;
    let isChecked = false;
    if (lodash.isArray(selectedContacts) && selectedContacts.length > 0) {
      const index2 = selectedContacts.findIndex((d) => d.id === item.id);
      if (index2 !== -1) {
        isChecked = true;
      }
    }
    return (
      <ContactRow
        key={item.id}
        action={onSelectContact}
        isChecked={isChecked}
        section={section}
        index={index}
        item={item}
        type={"newGroup"}
      />
    );
  };

  const renderNoHistory = () => {
    if (indicator.isLoadingRefreshControl !== true) {
      return <NoContact />;
    }
  };
  //------------------------------------------------------------------------
  const onBackPress = React.useCallback(() => {
    navigation.goBack();
  }, [navigation]);
  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={"dark-content"}
        backgroundColor={headerStyles.container.backgroundColor}
      />
      <Header
        style={headerStyles.container}
        containerStyle={headerStyles.container}
        centerContainerStyle={headerStyles.centerContainer}
        leftContainerStyle={headerStyles.sideContainer}
        rightContainerStyle={headerStyles.sideContainer}
        backgroundColor={headerStyles.container.backgroundColor}
        leftComponent={
          <TouchableRipple onPress={onBackPress}>
            <Text style={headerStyles.text}>{t("general.cancel")}</Text>
          </TouchableRipple>
        }
        centerComponent={
          <Text style={headerStyles.title}>
            {t("group_detail.add_members")}
          </Text>
        }
        rightComponent={
          <TouchableRipple onPress={onAddMember}>
            <Text style={headerStyles.text}>{t("general.add")}</Text>
          </TouchableRipple>
        }
      />
      <SectionList
        contentContainerStyle={styles.sectionContainer}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={indicator.isLoadingRefreshControl}
            onRefresh={refreshData}
          />
        }
        style={{}}
        sections={contact}
        renderSectionHeader={({ section: { key } }) => (
          <Text style={styles.sectionHeader}>{key}</Text>
        )}
        renderItem={renderItem}
        ListEmptyComponent={renderNoHistory()}
        onEndReached={onLoadMore}
        keyExtractor={(item, index) => `${item.id}_${index}`}
        extraData={contact}
      />
      <SafeAreaView />
    </View>
  );
};

export default AddMember;
