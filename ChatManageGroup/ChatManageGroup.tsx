import React, { useEffect, useMemo, useRef } from "react";
import { View, SafeAreaView, ScrollView, Text } from "react-native";
// import { useTranslation } from "react-i18next";
import { NavigationProp } from "@react-navigation/native";
import HeaderManageGroup from "./component/HeaderManageGroup";
// styles
import styles from "assets/styles/views/ChatManagerGroup/ChatManageGroup";
import MemberRow from "./component/MemberRow";

import { useDispatch, useSelector } from "react-redux";
// stores
import {
  // Actions
  groupDetail,
  // Selectors
  selectDataGroupDetail,
  removeGroupMember,
  selectConversations,
  selectUsers,
  selectLastOnline,
} from "store/slices/chatSlice";
import { ChatPerson } from "app/chatUtils";
import moment from "moment";
import { useTranslation } from "react-i18next";
import ModalEditGroup from "./component/ModalEditGroup";
import { TAB } from "app/constants";

type ChatManageGroupType = {
  navigation: NavigationProp<any>;
  route: {
    params: {
      data: { source: string | null; name: string; mute: boolean; id: string };
    };
  };
};

const ChatManageGroup = (props: ChatManageGroupType) => {
  const { t } = useTranslation();

  const editGroupModal = useRef<any>(null);
  const { navigation, route } = props;
  const rowRefs: any = React.useRef([]);
  const openRowKey = React.useRef(null);
  const {
    data: { id: conversationId },
  } = route?.params;

  const dispatch = useDispatch();
  const dataGroupDetail = useSelector(selectDataGroupDetail);
  const conversations = useSelector(selectConversations);
  const users = useSelector(selectUsers);
  const lastOnline: { id: string; lastSeen: number }[] =
    useSelector(selectLastOnline);

  //@ts-ignore
  const data = conversations.find((item) => item?.id === conversationId);

  //@ts-ignore
  const members: ChatPerson[] | undefined = useMemo(
    () =>
      users.filter((item: ChatPerson) => data?.members?.includes(item?.id)) ||
      undefined,
    [data?.members, users]
  );

  const recentMembers: ChatPerson[] = useMemo(() => {
    return (
      members?.filter((item) => {
        const last = lastOnline.find((i) => item.id === i.id);
        if (last) {
          const diff = moment().diff(moment(last.lastSeen), "days");
          return diff <= 30;
        }
        return false;
      }) || []
    );
  }, [lastOnline, members]);

  const idleMembers: ChatPerson[] = useMemo(() => {
    return (
      members?.filter((item) => {
        const last = lastOnline.find((i) => item.id === i.id);
        if (last) {
          const diff = moment().diff(moment(last.lastSeen), "days");
          return diff > 30;
        }
        return false;
      }) || []
    );
  }, [lastOnline, members]);

  useEffect(() => {
    //@ts-ignore
    dispatch(groupDetail(data?.source));
  }, [data?.source, dispatch]);

  const onRowRemove = React.useCallback(
    (userId: string) => () => {
      const groupId = dataGroupDetail?.id || "";
      // @ts-ignore
      const params = { groupId: groupId, userId: userId };
      // @ts-ignore
      dispatch(removeGroupMember(params));
      // navigation.goBack();
    },
    [dataGroupDetail?.id, dispatch]
  );

  useEffect(() => {
    if (data?.members?.length === 0) {
      navigation.navigate("Main", { screen: TAB.Chat });
    }
  }, [data?.members?.length, navigation]);

  const renderMember = (item: ChatPerson, index: number) => {
    return (
      <MemberRow
        data={members || []}
        item={item}
        index={index}
        onRowRemove={onRowRemove(item.id)}
        key={item.id}
        role="admin"
      />
    );
  };
  return (
    <View style={styles.bodyContainer}>
      <HeaderManageGroup
        navigation={navigation}
        data={dataGroupDetail}
        editGroupModal={editGroupModal}
        conversationId={data?.id}
      />
      <ScrollView style={styles.bodyContainer}>
        {recentMembers?.length !== 0 && recentMembers?.map(renderMember)}
        {idleMembers?.length !== 0 && (
          <>
            <Text style={styles.txtLabelList}>
              {t("group_detail.idle_members")}
            </Text>
            {idleMembers?.map(renderMember)}
          </>
        )}
      </ScrollView>
      <SafeAreaView style={styles.bodyColor} />
      <ModalEditGroup ref={editGroupModal} />
    </View>
  );
};

export default ChatManageGroup;
