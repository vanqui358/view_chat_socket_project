export const MockData = [
  {
    group: "A",
    data: [
      { name: "Bùi Thái An", id: 1 },
      { name: "Lê Việt Anh", id: 2 },
      { id: 3, name: "Trần Ngọc Ánh" },
    ],
  },
  {
    group: "B",
    data: [{ id: 4, name: "Benny" }],
  },
  {
    group: "H",
    data: [
      { id: 5, name: "Nguyễn Mạnh Hoàng" },
      { id: 6, name: "Võ Mạnh Hùng" },
      { id: 7, name: "Lý Ngọc Hương" },
    ],
  },
  {
    group: "K",
    data: [{ id: 8, name: "Kim" }],
  },
];
