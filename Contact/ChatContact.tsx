import React, { useEffect, useMemo } from "react";
import {
  View,
  SectionList,
  Keyboard,
  Animated,
  Easing,
  SafeAreaView,
  RefreshControl,
} from "react-native";
import { Text, TouchableRipple } from "react-native-paper";
// stores
import {
  listContacts,
  // Actions
  refetchContactAndUsers,
  // Selectors
  selectContacts,
  selectConversations,
  selectUsers,
} from "store/slices/chatSlice";
import {
  useFocusEffect,
  useIsFocused,
  useNavigation,
} from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import lodash from "lodash";
import { useTranslation } from "react-i18next";
import { makeFullname } from "app/helper";
import NewGroupIcon from "assets/images/svg/IconNewGroup.svg";
import NewContactIcon from "assets/images/svg/IconNewContact.svg";

import styles from "assets/styles/views/Contact/contactStyle";
import { SCREEN } from "utils/UtilDeviceInfo";

import { SCREENS } from "app/constants";

import NoContact from "components/ChatContact/NoContact";
import ContactHeader from "./component/ContactHeader";
import Line from "components/Line/Line";
import ContactRow from "components/ChatContact/ContactRow";
import { ChatPerson } from "app/chatUtils";

export const HeaderSectionButton = React.memo(
  ({
    icon: IC,
    title,
    action,
    isShowLine = true,
  }: {
    icon: React.FC;
    title: string;
    action: () => void;
    isShowLine?: boolean;
  }) => {
    return (
      <TouchableRipple
        style={styles.item}
        onPress={() => {
          action && action();
        }}
      >
        <>
          <View style={styles.leftHeaderItem}>
            <IC />
          </View>
          <View style={styles.rightItem}>
            <View style={styles.rightWrapper}>
              <Text numberOfLines={2} style={styles.headerSectionTitleBtn}>
                {title}
              </Text>
            </View>
            {isShowLine && <Line />}
          </View>
        </>
      </TouchableRipple>
    );
  }
);

const ChatContact = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const usersChat = useSelector(selectUsers);
  const contacts = useSelector(selectContacts);

  const conversations = useSelector(selectConversations);
  const [refreshing, setRefreshing] = React.useState(false);

  const isFocused = useIsFocused();
  const { navigate } = useNavigation();
  const { t } = useTranslation();
  const [contact, setContact] = React.useState<any[]>([]);

  //Animated
  const searchBarValue = React.useRef(new Animated.Value(SCREEN.width)).current;
  const opacityValue = React.useRef(new Animated.Value(1)).current;
  const [isShowView, setIsShowView] = React.useState(true);
  const searchTextRef = React.useRef("");

  const userNonInContact: ChatPerson[] = useMemo(
    () => usersChat.filter((item: ChatPerson) => !item.contact_added),
    [usersChat]
  );

  useEffect(() => {
    const map: any = {};
    contacts?.forEach((u: any) => {
      u.name = makeFullname(u.first_name, u.last_name);
      let c0 = u.name.charAt(0).toUpperCase();
      if (!/[A-Z]/.test(c0)) {
        c0 = "#";
      }
      if (!map[c0]) {
        map[c0] = [];
      }
      map[c0].push(u);
    });

    let groups: any[] = Object.keys(map).map((k) => ({
      key: k,
      data: map[k],
    }));
    groups = lodash.orderBy(groups, "key");
    groups.forEach((gr) => {
      gr.data = lodash.orderBy(gr.data, "name");
    });

    setContact(groups);
  }, [contacts]);

  const onHideSearchBar = React.useCallback(() => {
    Animated.sequence([
      Animated.timing(searchBarValue, {
        toValue: SCREEN.width,
        duration: 500,
        easing: Easing.linear,
        useNativeDriver: true,
      }),
      Animated.timing(opacityValue, {
        toValue: 1,
        duration: 800,
        easing: Easing.in(Easing.linear),
        useNativeDriver: true,
      }),
    ]).start(() => {
      setIsShowView(true);
    });
  }, [searchBarValue, opacityValue]);

  const _keyboardDidHide = React.useCallback(() => {
    if (lodash.isEmpty(searchTextRef.current)) {
      Keyboard.dismiss();
      onHideSearchBar();
    }
  }, [onHideSearchBar]);

  useEffect(() => {
    let keyboardHideListener = Keyboard.addListener(
      "keyboardDidHide",
      _keyboardDidHide
    );

    return () => {
      keyboardHideListener.remove();

      Animated.timing(searchBarValue, {
        toValue: SCREEN.width,
        useNativeDriver: false,
      }).stop();
      Animated.timing(opacityValue, {
        toValue: 1,
        useNativeDriver: false,
      }).stop();
    };
  }, [isFocused, opacityValue, searchBarValue, dispatch, _keyboardDidHide]);

  //---------------------------------HANDLE----------------------------

  //-----------------------------------------------------------------
  //--------------------------FETCH AND LOAD MORE------------------------

  const refreshData = React.useCallback(() => {
    setRefreshing(true);
    //@ts-ignore
    dispatch(refetchContactAndUsers({ callback: () => setRefreshing(false) }));
  }, [dispatch]);

  const renderItem = (itemProps: any) => {
    const { item, index, section } = itemProps;
    return (
      <ContactRow
        key={index.toString()}
        section={section}
        index={index}
        item={item}
        action={showEditContactPopup}
      />
    );
  };

  const renderListHeader = () => {
    return isShowView ? (
      <Animated.View style={[styles.actionGroup, { opacity: opacityValue }]}>
        <HeaderSectionButton
          icon={() => <NewGroupIcon width={29} height={28} />}
          title={t("chat_contact.new_group")}
          action={goToAddNewGroup}
        />
        <HeaderSectionButton
          icon={() => <NewContactIcon width={21} height={28} />}
          isShowLine={false}
          title={t("chat_contact.add_contact")}
          action={showAddContactPopup}
        />
      </Animated.View>
    ) : null;
  };

  //-------------------------Focus screen event--------------------------------
  useFocusEffect(
    React.useCallback(() => {
      refreshData();
    }, [refreshData])
  );
  //-------------------------ADD CONTACT--------------------------------
  const showAddContactPopup = React.useCallback(() => {
    //@ts-ignore
    navigate(SCREENS.AddFriend);
    dispatch(listContacts());
  }, [navigate, dispatch]);

  const showEditContactPopup = React.useCallback(
    (item) => {
      const conversation = conversations.find((i: { members?: string[],type:number }) => {
        console.log("u",i)
        return (
          i?.members &&
          i?.type !==1&&
          i?.members?.length === 2 &&
          i?.members?.includes(item?.id)
        );
      });
      console.log("conversation",conversation)
      !!conversation?.id &&
        //@ts-ignore
        navigate(SCREENS.ChatDetail, { conversationId: conversation?.id });
    },
    [conversations, navigate]
  );

  const goToAddNewGroup = React.useCallback(() => {
    //@ts-ignore
    navigate(SCREENS.ChatGroupCreate);
  }, [navigate]);

  const renderNoContact = () => {
    return <NoContact />;
  };

  const handleDataContact = useMemo(() => {
    if (userNonInContact.length === 0) {
      return contact;
    }
    return [
      {
        key: t("chat_contact.not_in_contact"),
        data: userNonInContact,
      },
      ...contact,
    ];
  }, [contact, t, userNonInContact]);

  //-------------------------------------------------------------------
  return (
    <View style={[styles.container]}>
      <ContactHeader
        name="l"
        onSearch={() => {}}
        title={t("chat_contact.header")}
        navigation={navigation}
      />
      <SectionList
        contentContainerStyle={[styles.sectionContainer]}
        ListHeaderComponent={renderListHeader}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={refreshData} />
        }
        style={[styles.sectionContainer]}
        sections={handleDataContact}
        keyExtractor={(item, index) => `${item}_${index}`}
        renderSectionHeader={({ section: { key } }) => (
          <Text style={styles.sectionHeader}>{key}</Text>
        )}
        renderItem={renderItem}
        ListEmptyComponent={renderNoContact}
        extraData={handleDataContact}
      />
      <SafeAreaView style={styles.sectionContainer} />
    </View>
  );
};

export default ChatContact;
