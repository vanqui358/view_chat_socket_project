import React from "react";
import { useTranslation } from "react-i18next";
import { Header } from "react-native-elements";
import { NavigationProp } from "@react-navigation/native";

// components
import StatusBar from "components/App/StatusBar";
import BackButton from "components/Button/BackButton";

// utils

// styles
import styles from "assets/styles/views/ProfilePractice/components/profilePracticeHeaderStyle";
import { View, Text } from "react-native";

type ScreenProps = {
  title: string;
  navigation: NavigationProp<any>;
  name: string;
  subtitle?: string;
  onSearch: (keywords: string | null) => void;
};

export default function ContactHeader(props: ScreenProps) {
  const { t } = useTranslation();
  const { title, navigation } = props;
  const bgColor = styles.container.backgroundColor;

  const onBackPress = React.useCallback(() => {
    navigation.goBack();
  }, [navigation]);
  return (
    <>
      <StatusBar barStyle={"dark-content"} backgroundColor={bgColor} />
      <Header
        style={styles.container}
        containerStyle={styles.container}
        centerContainerStyle={styles.centerContainer}
        leftContainerStyle={styles.sideContainer}
        rightContainerStyle={styles.sideContainer}
        backgroundColor={bgColor}
        leftComponent={
          <BackButton label={t("label.button_back")} onPress={onBackPress} />
        }
        centerComponent={<Text style={styles.title}>{title}</Text>}
        rightComponent={<View />}
      />
    </>
  );
}
