// @ts-ignore
import { NavigationProp } from "@react-navigation/native";
import { ChatMessage } from "app/chatUtils";
import { grayColor } from "assets/styles/theme";

// styles
import styles from "assets/styles/views/ProfilePractice/profilePracticeStyle";
import AccessoryMessageInput from "components/AccessoryMessageInput/AccessoryMessageInput";
import LastSeenText from "components/Text/LastSeenText";
import { useDeeplinkRouteData } from "hooks/views/App/deeplinkHook";
import React, { useCallback, useRef } from "react";
import { useTranslation } from "react-i18next";
import { BackHandler, FlatList, SafeAreaView, View } from "react-native";
import { Text } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { selectIsDeepLinkForBackground } from "store/slices/appSlice";
// components
// stores
import {
  deleteMessage,
  fetchMessages,
  groupDetail,
  readConversation,
  selectActiveConversationId,
  selectActiveConversationMessages,
  selectConversations,
  selectLastOnlineById,
  sendMessage,
  setActiveConversationId,
  selectUsers,
} from "store/slices/chatSlice";
import ChatContent from "./component/ChatContent";
import ChatHeader from "./component/ChatHeader";
import { GroupInvitation } from "./component/GroupInvitation";

type ScreenProps = {
  navigation: NavigationProp<any>;
  route: any;
};

const ChatDetail = (props: ScreenProps) => {
  const { t } = useTranslation();
  const { navigation, route } = props;
  const { conversationId } = route?.params;
  const conversations = useSelector(selectConversations);
  const usersChat = useSelector(selectUsers);
  const { handleGoBack } = useDeeplinkRouteData();
  const isDeepLinkForBackground = useSelector(selectIsDeepLinkForBackground);
  //@ts-ignore
  const data = conversations.find((item) => item.id === conversationId);
  const [nickNameChat,setNickNameChat] = React.useState();
    React.useEffect(() => {
      const userChat =  usersChat.find((item:any)=>item?.id === data?.name)
      if(userChat?.first_name==="" && userChat?.last_name===""){
        setNickNameChat(userChat?.nickname);
      }
  },[nickNameChat,usersChat, data?.name])
  const refFlatList = useRef<FlatList>(null);

  const lastOnline = useSelector((state) =>
    // @ts-ignore
    selectLastOnlineById(state, data?.source)
  );

  React.useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      () => {
        return isDeepLinkForBackground;
      }
    );
    return () => backHandler.remove();
  }, [isDeepLinkForBackground, handleGoBack]);

  const dispatch = useDispatch();
  const activeConversationId = useSelector(selectActiveConversationId);

  const infoMessage = useSelector(selectActiveConversationMessages);
  //@ts-ignore
  const messages = infoMessage?.messages;

  //@ts-ignore
  const seenAt = infoMessage?.seen_at || 0;

  React.useEffect(() => {
    dispatch(setActiveConversationId(conversationId));
    return function cleanUp() {
      dispatch(setActiveConversationId(null));
    };
  }, [dispatch, conversationId]);

  React.useEffect(() => {
    if (activeConversationId) {
      dispatch(fetchMessages());
    }
  }, [dispatch, activeConversationId]);


  React.useEffect(() => {
    if (activeConversationId && messages) {
      // @ts-ignore
      dispatch(readConversation({ conversationId: activeConversationId }));
    }
  }, [dispatch, activeConversationId, messages]);

  React.useEffect(() => {
    if (data?.type === 1) {
      //@ts-ignore
      dispatch(groupDetail(data?.source));
    }
  }, [dispatch, data?.type, data?.source]);

  const scrollToNewMessage = useCallback(() => {
    if (messages?.length !== 0) {
      refFlatList.current?.scrollToOffset({ offset: 0, animated: true });
    }
  }, [messages?.length]);

  const onSendMessage = React.useCallback(
    (message: any) => {
      dispatch(
        // @ts-ignore
        sendMessage({
          conversationId: data?.id,
          type: message.type,
          message: message.text,
        } as any)
      );
      scrollToNewMessage();
    },
    [dispatch, data?.id, scrollToNewMessage]
  );
  const onDeleteMessage = React.useCallback(
    (message?: ChatMessage) => {
      dispatch(
        // @ts-ignore
        deleteMessage({
          conversationId: message?.conversation_id,
          messageId: message?.id,
        } as any)
      );
    },
    [dispatch]
  );

  return (
    <View style={styles.container}>
      <ChatHeader
        title={nickNameChat?(nickNameChat):(data?.name)}
        navigation={navigation}
        idConversation={lastOnline?.id}
        conversationType={data?.type}
        subtitle={
          data?.type ? (
            <Text>
              {t("group_detail.number_members", {
                count: data?.members.length || "-",
              })}
            </Text>
          ) : (
            <LastSeenText lastSeen={lastOnline?.lastSeen} />
          )
        }
        data={data}
      />
      <View style={styles.containerChat}>
        <ChatContent
          refFlatList={refFlatList}
          data={messages}
          type={data?.type}
          seen_at={seenAt}
          deleteMessage={onDeleteMessage}
        />
        <GroupInvitation type={data?.type || 0} />
      </View>

      {
        <AccessoryMessageInput
          //@ts-ignore
          scrollToNewMessage={scrollToNewMessage}
          onSendMessage={onSendMessage}
          conversationId={data?.id}
          conversation={data?.type}
        />
      }
      <SafeAreaView style={styles.containerSafeAreaView} />
    </View>
  );
};

export default ChatDetail;
