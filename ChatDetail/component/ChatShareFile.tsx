import { ChatMessage } from "app/chatUtils";
import { getIcons } from "app/icons";
import { grayColor } from "assets/styles/theme";
import styles from "assets/styles/views/ChatDetail/component/chatShareFileStyle";
import PopupBase from "components/MyPopup/base/PopupBase";
import React, { memo, SetStateAction, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import {
  ActivityIndicator,
  Image,
  Platform,
  Pressable,
  TouchableOpacity,
  View,
  useWindowDimensions,
} from "react-native";
import fs from "react-native-fs";
import ImageViewer from "react-native-image-zoom-viewer";
import { Text } from "react-native-paper";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import PDFView from "react-native-view-pdf";
import { useSelector } from "react-redux";
import RNFetchBlob from "rn-fetch-blob";
import {
  DefaultBottomControlsBar,
  DefaultMainControl,
  VideoPlayer,
} from "react-native-true-sight";
import Video from "react-native-video";
import { selectProfile } from "store/slices/userSlice";

type ScreenProps = {
  item: ChatMessage;
  action: { showAction: () => void; hideActionMessage: () => void,onPressItemShareCollection:(item:any)=>void };
};

enum FileType {
  Unknown,
  Pdf,
  Video,
  Image
};

const IconClose = getIcons("icCloseModel");
const IconDownload = getIcons("icDownload");

const DOWNLOAD_PATH =
  Platform.OS === "ios"
    ? fs.DocumentDirectoryPath
    : "file://" + fs.DownloadDirectoryPath;

const ChatShareFile = (props: ScreenProps) => {
  const { t } = useTranslation();
  const [globalUrl, setGlobalUrl] = useState("");
  const [url, setUrl] = useState("");
  const [isExist, setIsExist] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [showModal, setShowModal] = useState(false);
  const [progress, setProgress] = useState(0);
  const [errorSaveFile, setErrorSaveFile] = useState(false);
  const { top } = useSafeAreaInsets();
  const profile = useSelector(selectProfile);
  const isMine = props.item.sender_id === profile.id;
  const { width: screenWidth, height: screenHeight } = useWindowDimensions();

  useEffect(() => {
    try {
      const result =JSON.parse(props.item.message)?.file||JSON.parse(props.item.message).url;
      setUrl(result);
      setGlobalUrl(result);
    } catch (error) {
      setUrl("");
      setGlobalUrl("");
      console.log("---------------------------");
      console.log("err parse message image");
      console.log("---------------------------");
    }
  }, [props.item.message]);

  const arrElementUrl = url.split("/");
  const folder = arrElementUrl[arrElementUrl.length - 2];
  const title = arrElementUrl[arrElementUrl.length - 1];
  const isPDF = title.includes("pdf");
  const savePath = DOWNLOAD_PATH + "/" + folder + "/" + title;

  const fileType = React.useMemo(() => {
    if (!url) return FileType.Unknown;
    const lowerUrl = url.toLowerCase();
    if (lowerUrl.includes(".pdf")) return FileType.Pdf;
    if (lowerUrl.includes(".mov") || lowerUrl.includes(".avi") || lowerUrl.includes(".mp4") || lowerUrl.includes(".flv") || lowerUrl.includes(".mkv")) return FileType.Video;
    return FileType.Image;
  }, [url]);
  useEffect(() => {
    if (props.item.id !== "LOADING_ID" && !isPDF) {
      setIsLoading(false);
    }
  }, [isPDF, props.item.id]);

  useEffect(() => {
    if (savePath.length !== 0) {
      const checkExist = async () => {
        const res = await fs.exists(savePath);
        if (res) {
          setUrl(savePath);
          setIsExist(true);
        }
      };
      checkExist();
    }
  }, [savePath, isLoading]);

  if (url.length === 0) {
    return null;
  }

  const onPress = async () => {
    const item = {
      chat_id:props.item?.id,
      item_id:JSON.parse(props.item.message)?.id,
      conversation_id:props.item?.conversation_id,
    }
    props.action.onPressItemShareCollection && props.action.onPressItemShareCollection(item);
    props.action.hideActionMessage && props.action.hideActionMessage();
    setShowModal(true);
  };

  const onDownloadPress = async () => {
    setIsLoading(true);
    setErrorSaveFile(false);
    try {
      const resp = await RNFetchBlob.config({
        fileCache: true,
        path: savePath,
        addAndroidDownloads: {
          useDownloadManager: true,
          path: savePath,
        },
      })
        .fetch("GET", url)
        .progress((received: number, total: number) => {
          setProgress((received * 100) / total);
        });
      const newUrl = resp.path();
      setUrl(newUrl);
      setIsExist(true);
    } catch (error) {
      console.log("---------------------------");
      console.log("can not save file: ", error);
      console.log("---------------------------");
    }
    if (savePath.length !== 0) {
      const checkExist = async () => {
        const res = await fs.exists(savePath);
        if (!res) {
          setErrorSaveFile(true);
        }
      };
      checkExist();
    }
    setIsLoading(false);
  };

  const onCloseModalPress = () => {
    setShowModal(false);
  };

  const isMustDownload = !(isExist || isMine);

  return (
    <>
      <Pressable
        disabled={isMustDownload}
        onPress={onPress}
        onLongPress={props.action.showAction}
        style={styles.container}
      >
        <Thumbnail
          errorSaveFile={errorSaveFile}
          globalUrl={globalUrl}
          isLoading={isLoading}
          isMustDownload={isMustDownload}
          onDownloadPress={onDownloadPress}
          progress={progress}
          isMine={isMine}
          setUrl={setUrl}
          url={url}
          fileType={fileType}
        />
        <View style={styles.wrapperTitle}>
          <Text numberOfLines={2} style={styles.title}>
            {title}
          </Text>
          <Text style={styles.txtFail}>
            {errorSaveFile && t("chat.attach.download_file_failure")}
          </Text>
        </View>
      </Pressable>
      <PopupBase isShow={showModal} onRequestClose={onCloseModalPress}>
        <View style={styles.containerModal}>
          {fileType ===FileType.Pdf && (
            <>
              <View style={[{ marginTop: top + 10 }, styles.wrapperBtnClose]} />
              <PDFView
                resource={url}
                style={{ flex: 1 }}
                resourceType={"url"}
                onLoad={() => console.log(`PDF rendered from url`)}
                onError={(error) => console.log("Cannot render PDF", error)}
              />
            </>
          )}
          {fileType === FileType.Image && (
            <ImageViewer
              imageUrls={[{ url }]}
              renderIndicator={() => <View />}
              style={{ flex: 1 }}
            />
          )}
          {fileType === FileType.Video && (
            <VideoPlayer
              autoStart
              mainControl={(args) => <DefaultMainControl {...args} />}
              bottomControl={(args) => <DefaultBottomControlsBar {...args} />}
            >
              {(args) => (
                <Video
                  ref={args.playerRef}
                  source={{ uri: encodeURI(url) }}
                  paused={args.videoPaused}
                  onLoad={args.onLoad}
                  onProgress={(data) => {
                    const meta = {
                      ...data,
                      playableDuration: data.seekableDuration,
                    };
                    args.onProgress(meta);
                  }}
                  onEnd={args.onEnd}
                  style={{ width: screenWidth, height: screenHeight }}
                  resizeMode="contain"
                />
              )}
            </VideoPlayer>
          )}  
          <TouchableOpacity
            onPress={onCloseModalPress}
            style={[styles.btnClose, { top: 10 + top }]}
          >
            <IconClose width={24} height={24} />
          </TouchableOpacity>
        </View>
      </PopupBase>
    </>
  );
};

const Thumbnail = ({
  isLoading,
  progress,
  isMustDownload,
  errorSaveFile,
  onDownloadPress,
  url,
  globalUrl,
  isMine,
  fileType,
  setUrl,
}: {
  url: string;
  isMine: boolean;
  isLoading: boolean;
  progress: number;
  isMustDownload: boolean;
  errorSaveFile: boolean;
  onDownloadPress: () => void;
  globalUrl: string;
  setUrl: React.Dispatch<SetStateAction<string>>;
  fileType: FileType,
}) => {

  return (
    <View style={styles.thumbnail}>
      {!isMine && isLoading ? (
        <Loading progress={progress} />
      ) : isMustDownload || errorSaveFile ? (
        <TouchableOpacity onPress={onDownloadPress}>
          <IconDownload width={28} height={28} />
        </TouchableOpacity>
      ) :
        <>
          {fileType === FileType.Pdf && (
            <View pointerEvents="none">
              <PDFView
                resource={url}
                style={styles.thumbnailPdf}
                resourceType={"url"}
                onError={(error) => {
                  setUrl(globalUrl);
                  console.log("Cannot render PDF", error);
                }}
              />
            </View>
          )}
          {fileType === FileType.Image && (
            <View pointerEvents="none">
              <Image
                source={{ uri: url }}
                style={styles.thumbnailPdf}
              />
            </View>
          )}
          {fileType === FileType.Video && (
            <View pointerEvents="none">
              <Video
                source={{ uri: encodeURI(url) }}
                resizeMode="cover"
                muted
                rate={0.0}
                style={styles.thumbnailPdf}
              />
            </View>
          )}
        </>
      }
    </View>
  );
};

const Loading = ({ progress }: { progress: number }) => {
  if (progress === 0 || progress === 100) {
    return <ActivityIndicator color={grayColor[3]} size={35} />;
  }
  return <Text style={styles.progressText}>{progress.toFixed()}%</Text>;
};

export default memo(ChatShareFile, (prevProps, nextProps) => {
  return prevProps.item.message === nextProps.item.message;
});
