import { fontBold, fontRegular, grayColor } from "assets/styles/theme";
import React from "react";
import { useTranslation } from "react-i18next";
import { StyleSheet, View, Text } from "react-native";
interface Props {
  width: number;
  height: number;
}
export const EmptyView = ({ width, height }: Props) => {
  const { t } = useTranslation();
  return (
    <View style={[styles.container, { height, width }]}>
      <Text style={styles.message}>{t("chat.empty.no_message_here_yet")}</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    transform: [{ rotate: `${Math.PI}rad` }, { rotateY: `${Math.PI}rad` }],
  },
  message: {
    fontFamily: fontRegular,
    fontSize: 18,
    color: "#8E8E93",
    textAlign: "center",
    marginTop: 10,
  },
});
