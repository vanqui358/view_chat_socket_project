import React from "react";
import { useNavigation } from "@react-navigation/native";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
//style
import {
  blueColor,
  redColor,
  fontRegular,
  blackColor,
  grayColor,
} from "assets/styles/theme";
//slice
import {
  createContact,
  selectUnArchives,
  archiveConversation,
} from "store/slices/chatSlice";
import { useDispatch, useSelector } from "react-redux";

type ScreenProps = {
  idConversation: string;
  name: string;
  contacts: any;
  conversationType: number;
};

const ContactInvitation = (props: ScreenProps) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const conversations = useSelector(selectUnArchives);

  let conversation = "";
  for (let i = 0; i < conversations.length; i++) {
    if (conversations[i].source === props.idConversation) {
      conversation = conversations[i].id;
    }
  }
  const contactInfo = React.useMemo(
    () => ({
      username: props.idConversation,
      first_name: props.name,
      last_name: "",
      phone: "",
      avatar: null,
    }),
    [props.idConversation, props.name]
  );

  for (let i = 0; i < props.contacts.length; i++) {
    if (
      props.idConversation === props.contacts[i].id
    ) {
      return null;
    }
  }
  const onAddPress = () => {
    dispatch(
      //@ts-ignore
      createContact({ contactInfo })
    );
  };

  const onBlockPress = () => {
    //@ts-ignore
    dispatch(archiveConversation({ conversationId: conversation }));
    navigation.goBack();
  };
  if(conversation.includes("__tete")){
    return null;
  }
  return (
    <View style={styles.confirmAddContact}>
      <Text style={styles.txtSubTitle}>This user is not on your contacts.</Text>
      <TouchableOpacity onPress={onBlockPress}>
        <Text style={[styles.txtBtn, { color: redColor[0] }]}>Block</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={onAddPress}>
        <Text style={[styles.txtBtn, { color: blueColor[0] }]}>Add</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  txtBtn: { fontFamily: fontRegular, fontSize: 16, marginLeft: 30 },
  txtSubTitle: {
    fontFamily: fontRegular,
    fontSize: 17,
    color: blackColor,
    flex: 1,
  },
  confirmAddContact: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#F2F2F7",
    borderTopColor: grayColor[3],
    borderTopWidth: 1,
    paddingHorizontal: 16,
    height: 50,
  },
});
export default ContactInvitation;
