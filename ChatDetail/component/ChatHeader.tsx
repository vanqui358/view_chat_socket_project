import React, { ReactNode } from "react";
import { useTranslation } from "react-i18next";
import { Header } from "react-native-elements";
import { useSelector } from "react-redux";
import { NavigationProp } from "@react-navigation/native";
import { SCREENS } from "app/constants";

// components
import StatusBar from "components/App/StatusBar";
import BackButton from "components/Button/BackButton";
import ContactInvitation from "./ContactInvitation";
// store
import { selectProfile } from "store/slices/userSlice";
import { selectContacts } from "store/slices/chatSlice";
// utils

// styles
import styles from "assets/styles/views/ProfilePractice/components/profilePracticeHeaderStyle";
import { View, Text } from "react-native";
import Avatar from "components/GeneralAvatar/Avatar";
import { useDeeplinkRouteData } from "hooks/views/App/deeplinkHook";

type ScreenProps = {
  title: string;
  idConversation: string;
  conversationType: number;
  navigation: NavigationProp<any>;
  subtitle?: ReactNode;
  data: any;
};

const ChatHeader = (props: ScreenProps) => {
  const { t } = useTranslation();
  const { title, navigation, subtitle, data,conversationType } = props;
  const profile = useSelector(selectProfile);
  const contacts = useSelector(selectContacts);
  const { handleGoBack } = useDeeplinkRouteData();

  const bgColor = styles.container.backgroundColor;
  const onOpenProfileRequest = () => {
    if (data.type === 1) {
      navigation.navigate(SCREENS.ChatGroupDetail, { data: data });
    } else {
      const userId = data?.members.filter((e: string) => e !== profile.id)?.[0];
      if (userId) navigation.navigate(SCREENS.ChatContactDetail, { userId });
    }
  };
  const centerComponent = () => {
    return (
      <View style={styles.vCenter}>
        <Text style={styles.title}>{title}</Text>
        {subtitle}
      </View>
    );
  };

  return (
    <>
      <StatusBar barStyle={"dark-content"} backgroundColor={bgColor} />
      <Header
        style={styles.container}
        containerStyle={styles.container}
        centerContainerStyle={styles.centerContainer}
        leftContainerStyle={styles.sideContainer}
        rightContainerStyle={styles.sideContainer}
        backgroundColor={bgColor}
        leftComponent={
          <BackButton label={t("general.back")} onPress={handleGoBack()} />
        }
        centerComponent={centerComponent()}
        rightComponent={
          <Avatar
            style={styles.avatar}
            text={title}
            image={data?.avatar}
            onPress={onOpenProfileRequest}
          />
        }
      />
      {conversationType===1?(null):( <ContactInvitation
        idConversation={props.idConversation}
        contacts={contacts}
        name={data?.name}
        conversationType={props.conversationType}
      />)}
   
    </>
  );
};
export default ChatHeader;
