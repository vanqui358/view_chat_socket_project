import { ChatMessage } from "app/chatUtils";
import { getIcons } from "app/icons";
import styles from "assets/styles/views/ChatDetail/component/chatShareImageStyle";
import PopupBase from "components/MyPopup/base/PopupBase";
import React, { memo, useEffect, useState } from "react";
import {
  ActivityIndicator,
  Image,
  Pressable,
  TouchableOpacity,
  View,
} from "react-native";
import FastImage from "react-native-fast-image";
import ImageViewer from "react-native-image-zoom-viewer";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { Error } from "./Error";

type ScreenProps = {
  item: ChatMessage;
  action: { showAction: () => void; hideActionMessage: () => void };
};

const IconClose = getIcons("icCloseModel");

const ChatShareImage = (props: ScreenProps) => {
  const [url, setUrl] = useState("");
  const [isError, setError] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const { top } = useSafeAreaInsets();

  useEffect(() => {
    try {
      setError(false);
      const result = JSON.parse(props.item.message).url;
      setUrl(result);
    } catch (error) {
      setUrl("none");
      console.log("---------------------------");
      console.log("err parse message image");
      console.log("---------------------------");
    }
  }, [props.item.message, isError]);

  const onError = () => {
    setError(true);
  };
  if (isError) {
    return <Error />;
  }
  const onPress = async () => {
    props.action.hideActionMessage && props.action.hideActionMessage();
    setShowModal(true);
  };

  const onCloseModalPress = () => {
    setShowModal(false);
  };
  return (
    <>
      <View style={styles.container}>
        {props.item.id === "LOADING_ID" ? (
          <>
            {url.includes("://") && (
              <Image
                source={{ uri: url, cache: "force-cache" }}
                style={styles.imageLoading}
                resizeMode="cover"
              />
            )}
          
          </>
        ) : (
          <Pressable onPress={onPress} onLongPress={props.action.showAction}>
            {url.includes("://") && (
              <FastImage
                source={{ uri: url, cache: "immutable" }}
                style={styles.image}
                onError={onError}
                resizeMode="cover"
              />
            )}
          </Pressable>
        )}
      </View>
      <PopupBase isShow={showModal} onRequestClose={onCloseModalPress}>
        <ImageViewer
          imageUrls={[{ url }]}
          renderIndicator={() => <View />}
          style={{ flex: 1 }}
        />
        <TouchableOpacity
          onPress={onCloseModalPress}
          style={[styles.btnClose, { top: 10 + top }]}
        >
          <IconClose width={24} height={24} />
        </TouchableOpacity>
      </PopupBase>
    </>
  );
};
export default memo(ChatShareImage, (prevProps, nextProps) => {
  return prevProps.item.message === nextProps.item.message;
});
