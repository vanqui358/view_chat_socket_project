import React from "react";

import { Linking, Pressable, Text, View } from "react-native";

import FastImage from "react-native-fast-image";
import { PreviewData } from "@flyerhq/react-native-link-preview";

import styles from "assets/styles/views/ChatDetail/component/chatContentStyle";
import { shareDeepLink } from "store/slices/appSlice";
import { useDispatch } from "react-redux";

interface Props {
  previewData: PreviewData | undefined;
}
export const PreviewLink = ({ previewData }: Props) => {
  const dispatch =  useDispatch();
  if (previewData?.title || previewData?.image || previewData?.description) {
    return (
      <Pressable
        onPress={async () => {
          // await Linking.openURL(previewData?.link || "");
          dispatch(shareDeepLink(previewData?.link || ""))
        }}
        style={styles.containerPreviewLink}
      >
        <View style={styles.verticalBarPreviewLink} />
        <View style={styles.contentPreviewLink}>
          {previewData?.image?.url && (
            <FastImage
              source={{ uri: previewData?.image?.url }}
              style={styles.imagePreviewLink}
            />
          )}
          {previewData?.title && (
            <Text style={styles.titlePreviewLink}>{previewData?.title}</Text>
          )}
          {previewData?.description && (
            <Text style={styles.descriptionPreviewLink}>
              {previewData?.description}
            </Text>
          )}
        </View>
      </Pressable>
    );
  }
  return null;
};
