import { useNavigation } from "@react-navigation/native";
import { ChatMessage } from "app/chatUtils";
import styles from "assets/styles/views/ChatDetail/component/chatShareScoreStyle";
import React from "react";
import { useTranslation } from "react-i18next";
import { Pressable, View } from "react-native";
import { Text } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { selectProfile } from "store/slices/userSlice";
import { getIcons } from "app/icons";
import { fetchCategories, receiveCollection, selectReceiveCategoriesCollection } from "store/slices/categorySlice";
import { TAB, SCREENS } from "app/constants";
import { sendRequest } from "store/slices/appSlice";
import { apiTapItemShareCollections } from "api/shareApi";
type ScreenProps = {
  item: ChatMessage;
  action: { showAction: () => void; hideActionMessage: () => void; onPressItemShareCollection: (item: any) => void };
  type?: number,
};

type SharedCollectionMessageType = {
  id: number;
  name: string;
  ref_id: number;
  score_id: number;
  shared_items?: { [id: string]: string };
};

const IconScoreInChat = getIcons("IconScoreInChat");

const ChatShareCollection = (props: ScreenProps) => {
  const navigation = useNavigation();
  const collectionInfo = React.useMemo<SharedCollectionMessageType>(
    () => JSON.parse(props.item.message),
    [props.item.message]
  );
  // useSelector
  const profile = useSelector(selectProfile);
  const receiveCollec = useSelector(selectReceiveCategoriesCollection);

  const dispatch = useDispatch();
  React.useEffect(() => {
    if (receiveCollec?.item_id) {
      navigation.navigate('Main', {
        screen: TAB.Scores,
      })
    }
  }, [navigation, receiveCollec])

  const onPress = React.useCallback(async () => {
    const item = {
      chat_id: props.item?.id,
      item_id: props.item?.ref_id,
      conversation_id: props.item?.conversation_id,
    }

    props.action.onPressItemShareCollection && props.action.onPressItemShareCollection(item);
    props.action.hideActionMessage();
    dispatch(fetchCategories());
    //@ts-ignore
    dispatch(receiveCollection(item));
    // const response = await sendRequest(
    //   dispatch,
    //   async (accessToken: any) => await apiTapItemShareCollections(item, accessToken)
    // );
    // console.log("responseasdasd", response)
    // navigation.navigate(SCREENS.ScoreList, { category_id: response?.data?.item_id });
  }, [props.item?.id, props.item?.ref_id, props.item?.conversation_id, props.action, dispatch]);





  const isMine = profile.id === props.item.sender_id;

  return (
    <Pressable
      onPress={onPress}
      onLongPress={props.action.showAction}
      style={[
        styles.container,
        {
          paddingRight: isMine ? 25 : 10,
        },
      ]}
    >
      {/* {!score && <ActivityIndicator size={32} color={"white"} />} */}
      <>
        <View style={styles.iconScore}>
          <IconScoreInChat with={15} height={20} />
        </View>
        <View style={styles.wrapperTextInfo}>
          <Text style={styles.txtTitle}>{collectionInfo?.name}</Text>
          <Text style={styles.txtNameAuthor}></Text>
        </View>

      </>
    </Pressable>
  );
};
export default ChatShareCollection;
