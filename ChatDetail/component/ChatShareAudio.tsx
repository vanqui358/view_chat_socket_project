import { ChatMessage } from "app/chatUtils";
import { formatDuration } from "app/format";
import { getIcons } from "app/icons";
import styles from "assets/styles/views/ChatDetail/component/chatShareAudioStyle";
import { usePlayAudio } from "hooks/components/Audio/playbackAudioControllerHook";
import React, { memo, useEffect, useState } from "react";
import { ActivityIndicator, Pressable, TouchableOpacity, View } from "react-native";
import { Text } from "react-native-paper";
import { useSelector } from "react-redux";
import { selectProfile } from "store/slices/userSlice";

type ScreenProps = {
  item: ChatMessage;
  action:{ onPressItemShareCollection:(item:any)=>void }
};

const BtnPlay = getIcons("btnPlay");
const BtnPause = getIcons("btnPause");
const ChatShareAudio = (props: ScreenProps) => {
  const [url, setUrl] = useState("");
  const arrElementUrl = url.split("/");
  const profile = useSelector(selectProfile);

  const { msDuration, isPlaying, onPlayPress, currentTime } = usePlayAudio({
    file:url,
  });

  const onPress = () => {
    const item = {
      chat_id:props.item?.id,
      item_id:JSON.parse(props.item.message)?.id,
      conversation_id:props.item?.conversation_id,
    }
    props.action.onPressItemShareCollection && props.action.onPressItemShareCollection(item);
  }

  useEffect(() => {
    try {
      const result = JSON.parse(props.item.message).file || JSON.parse(props.item.message).url as string;
      setUrl(result);
    } catch (error) {
      setUrl("");
      console.log("---------------------------");
      console.log("Error when parse audio message");
      console.log("---------------------------");
    }
  }, [props.item.message]);

  const Icon = isPlaying ? BtnPause : BtnPlay;
  const isMine = profile.id === props.item.sender_id;
  return (
    <Pressable
    onPress={onPress}
      style={[
        styles.container,
        {
          paddingRight: isMine ? 25 : 10,
        },
      ]}
    >
      <View style={styles.wrapperContent}>
        {props.item.id === "LOADING_ID" ? (
          <ActivityIndicator
            size={50}
            color={"black"}
            style={styles.indicator}
          />
        ) : (
          <TouchableOpacity onPress={onPlayPress}>
            <Icon width={40} height={40} />
          </TouchableOpacity>
        )}
        <View style={styles.wrapperTitle}>
          <Text numberOfLines={1} style={styles.title}>
            {arrElementUrl[arrElementUrl.length - 1]}
          </Text>
          <Text style={styles.currentTime}>{formatDuration(currentTime)}</Text>
        </View>
        <Text style={styles.duration}>{formatDuration(msDuration / 1000)}</Text>
      </View>
    </Pressable>
  );
};
export default memo(ChatShareAudio, (prevProps, nextProps) => {
  return prevProps.item.message === nextProps.item.message;
});
