/* eslint-disable react-hooks/exhaustive-deps */
import { ChatMessage } from "app/chatUtils";
import { getIcons } from "app/icons";
import styles from "assets/styles/views/ChatDetail/component/chatShareVideoStyle";
import PopupBase from "components/MyPopup/base/PopupBase";
import React, { useEffect, useState, useRef } from "react";
import {
  ActivityIndicator,
  Pressable,
  TouchableOpacity,
  View,
  StyleSheet,
  Platform,
  Text,
  Dimensions,
} from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import {
  DefaultBottomControlsBar,
  DefaultMainControl,
  // VideoPlayer,
} from "react-native-true-sight";
import Video from "react-native-video";
// import convertToProxyURL from "react-native-video-cache";
import { Error } from "./Error";
import VideoPlayer from "react-native-video-player";
import { SIZES } from "views/SightReading/data/Constant";
import { useDispatch, useSelector } from "react-redux";
import { fetchContentDetailItem, selectContentItem, selectDetail } from "store/slices/contentSlice";
import { IContentItemBase, IContentItemVideo } from "types/Content";
import { SCREENS } from "app/constants";
import { useNavigation } from "@react-navigation/native";

type ScreenProps = {
  item: ChatMessage;
  action: { showAction: () => void; hideActionMessage: () => void };
};

const IconClose = getIcons("icCloseModel");
const BtnPlay = getIcons("btnPlay");
const ChatShareVideo = (props: ScreenProps) => {
  const [isShow, setIsShow] = useState(false);
  const { top } = useSafeAreaInsets();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [uri, setUri] = useState("none");
  const [isError, setError] = useState(false);
  const deviceWidth = Dimensions.get("window").width;
  const maxWidth = (deviceWidth - 20) * 0.8;
  const videoRef = useRef<Video | null>();
  //@ts-ignore
  const contentItem: IContentItemVideo | null = useSelector((state) =>
    selectContentItem(state, JSON.parse(props.item.message)?.uuid)
  );

  const onOffModal = () => {
    props.action.hideActionMessage && props.action.hideActionMessage();
    setIsShow((prev) => !prev);
  };
  useEffect(() => {
    try {
      setError(false);
      const result = encodeURI(JSON.parse(props.item.message)?.file || JSON.parse(props.item.message).url);
      // console.log(convertToProxyURL(result));
      setUri(result);
      setTimeout(() => { }, 200);
    } catch (error) {
      setUri("none");
      console.log("---------------------------");
      console.log("Error when parse video message");
      console.log("---------------------------");
    }
  }, [props.item.message]);

  const onError = () => {
    setError(true);
  };
  const onLoad = () => {
    setError(false);
  };
  if (isError) {
    return <Error />;
  }
  const onRequestClose = () => setIsShow(false);
  return (
    <>
      {contentItem ? (
        <Pressable
           onPress={()=> // @ts-ignore
            navigation.navigate(SCREENS.VideoDetail, {
              uri: JSON.parse(props.item.message)?.uuid,
            })}
          //  onLongPress={props.action.showAction}
          style={[
            styles.containerVideoApp,
            {
              paddingRight: 25
            },
          ]}
        >
          <View style={{ marginBottom:10, position: "relative" }}>
            <>
              {uri?.includes("://") && (
                <Video
                  // ref={ref => videoRef.current = ref} 
                  source={{ uri }}
                  rate={0.0}
                  onError={onError}
                  onLoad={onLoad}
                  style={{ width: maxWidth, height: 230 }}
                  resizeMode="cover"
                  fullscreen={Platform.OS === "ios" ? isShow : false}
                  onFullscreenPlayerDidDismiss={onOffModal}
                />
              )}

              <View style={styles.indicator} pointerEvents="none">
                <BtnPlay />
              </View>
            </>

          </View>
            <View style={styles.wrapperTextInfo}>
              <Text style={styles.txtTitle}>{contentItem?.name}</Text>
              <Text style={styles.txtNameAuthor}>{contentItem?.content}</Text>
            </View>
        </Pressable>
      ) : (
        <View style={styles.container}>
          {props.item.id === "LOADING_ID" ? (
            <>
              {uri?.includes("://") && (
                <Video
                  source={{ uri }}
                  resizeMode="cover"
                  muted
                  rate={0.0}
                  style={styles.videoLoading}
                />
              )}
              <ActivityIndicator
                size={50}
                color={"white"}
                style={styles.indicator}
              />
            </>
          ) : (
            <>
              <Pressable
                onPress={onOffModal}
                onLongPress={props.action.showAction}
              >
                {uri?.includes("://") && (
                  <Video
                    // ref={ref => videoRef.current = ref} 
                    source={{ uri: uri }}
                    rate={0.0}
                    onError={onError}
                    onLoad={onLoad}
                    style={styles.video}
                    resizeMode="cover"
                    fullscreen={Platform.OS === "ios" ? isShow : false}
                    onFullscreenPlayerDidDismiss={onOffModal}
                  />
                )}
              </Pressable>
              <View style={styles.indicator} pointerEvents="none">
                <BtnPlay />
              </View>
            </>
          )}
        </View>

      )}
      {!(Platform.OS === "ios") ? (
        <PopupBase
          isShow={isShow}
          onRequestClose={onRequestClose}
          contentStyle={style.modalStyle}
        >
          {/* <VideoPlayer
          autoStart
          mainControl={(args) => <DefaultMainControl {...args} />}
          bottomControl={(args) => <DefaultBottomControlsBar {...args} />}
        >
          {(args) => (
            <Video
              controls
              ref={args.playerRef}
              source={{ uri: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4" }}
              paused={args.videoPaused}
              onLoad={args.onLoad}
              onProgress={(data) => {
                const meta = {
                  ...data,
                  playableDuration: data.seekableDuration,
                };
                args.onProgress(meta);
              }}
              onEnd={args.onEnd}
              style={styles.videoFull}
              resizeMode="contain"
              // fullscreen={true}
            />
          )}
        </VideoPlayer> */}
          <VideoPlayer
            video={{ uri }}
            videoWidth={SIZES.androidWidth.window}
            videoHeight={SIZES.androidHeightWithStatusBar.window}
            // controls
            showDuration={true}
            autoplay={true}
          // fullscreen={true}
          />
          <TouchableOpacity
            onPress={onOffModal}
            style={{ position: "absolute", top: top + 20, right: 5 }}
          >
            <IconClose width={24} height={24} />
          </TouchableOpacity>
        </PopupBase>
      ) : <></>}
    </>
  );
};
export default ChatShareVideo;

const style = StyleSheet.create({
  modalStyle: {
    margin: 0,
    backgroundColor: "black",
  },
});
