import { useNavigation } from "@react-navigation/native";
import { ChatMessage } from "app/chatUtils";
import { formatDuration } from "app/datetime";
import { SCREENS } from "app/constants";
import styles from "assets/styles/views/ChatDetail/component/chatShareRecordStyle";
import Avatar from "components/GeneralAvatar/Avatar";
import React, { useCallback } from "react";
import { ActivityIndicator, Pressable, View, ViewStyle } from "react-native";

import { Text } from "react-native-paper";
import { useSelector } from "react-redux";
import { selectUsersForShareRecord } from "store/slices/chatSlice";
import { selectRecordById } from "store/slices/recordsSlice";
import { selectProfile } from "store/slices/userSlice";
import { useScoreInfoStaticData } from "hooks/data/scores/scoresDataHook";

type ScreenProps = {
  item: ChatMessage;
  action: { showAction: () => void; hideActionMessage: () => void };
};

type SharedRecordMessageType = {
  id: number;
  name: string;
  shared_items?: { [id: string]: string };
};

const ChatShareRecord = (props: ScreenProps) => {
  const navigation = useNavigation();
  const recordInfo = React.useMemo<SharedRecordMessageType>(
    () => JSON.parse(props.item.message),
    [props.item.message]
  );
  const recordId = recordInfo.id;
  // useSelector
  const profile = useSelector(selectProfile);
  const users = useSelector(selectUsersForShareRecord);
  const record = useSelector((state) =>
    //@ts-ignore
    selectRecordById(state, { recordId, instanceId })
  );

  // fix author_name

  const user = users.find((u: any) => u.id === record?.author_id);
  const name = user?.first_name + user?.last_name;

  const score = useScoreInfoStaticData(record?.score_id);
  const instanceId = React.useMemo<string | null>(() => {
    if (!recordInfo || !profile || !recordInfo.shared_items) return null;
    return recordInfo.shared_items[profile.id];
  }, [profile, recordInfo]);

  const onPress = useCallback(() => {
    if (!record || !score) return;
    //@ts-ignore
    navigation.navigate(SCREENS.RecordDetail, {
      id: record.id,
      scoreId: record.score_id,
      scoreName: score.name,
      authorAvatar: record.author_avatar,
      instanceId: instanceId,
    });
    props.action.hideActionMessage && props.action.hideActionMessage();
  }, [navigation, props.action, record, score, instanceId]);

  const isMine = profile.id === props.item.sender_id;

  return (
    <Pressable
      onPress={onPress}
      onLongPress={props.action.showAction}
      style={[
        styles.container,
        {
          paddingRight: isMine ? 25 : 10,
        },
      ]}
    >
      {!record && <ActivityIndicator size={32} color={"white"} />}
      {record && (
        <>
          <Avatar text={name} size={45} image={record?.author_avatar} />
          <View style={styles.wrapperTextInfo}>
            <Text numberOfLines={2} style={styles.txtTitle}>
              {score?.name}
            </Text>
            <Text numberOfLines={2} style={styles.txtNameAuthor}>
              {name ? name : record?.author_id}
            </Text>
          </View>
          <Text style={styles.txtDuration}>
            {formatDuration(record.duration)}
          </Text>
        </>
      )}
    </Pressable>
  );
};
export default ChatShareRecord;
