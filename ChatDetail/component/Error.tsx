/* eslint-disable react-hooks/exhaustive-deps */
import { getIcons } from "app/icons";
import { fontRegular, grayColor } from "assets/styles/theme";
import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Dimensions, StyleSheet, Text, View } from "react-native";
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from "react-native-reanimated";
interface Props {}
const deviceWidth = Dimensions.get("window").width;
const maxWidth = (deviceWidth - 20) * 0.8;

const IconWarning = getIcons("icWarning");
export const Error = () => {
  const { t } = useTranslation();
  const opacity = useSharedValue(0);
  useEffect(() => {
    opacity.value = withTiming(1);
  }, []);

  const aniStyle = useAnimatedStyle(() => ({
    opacity: opacity.value,
    alignItems: "center",
  }));
  return (
    <View style={styles.container}>
      <Animated.View style={aniStyle}>
        <IconWarning width={32} height={32} />
        <Text style={styles.txtErr}>{t("chat.file.error")}</Text>
      </Animated.View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    height: maxWidth,
    width: maxWidth,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: grayColor[0],
  },
  txtErr: {
    fontFamily: fontRegular,
    fontSize: 12,
    textAlign: "center",
    color: grayColor[2],
    marginTop: 5,
  },
});
