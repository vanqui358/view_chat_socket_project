/* eslint-disable react-native/no-inline-styles */
import Clipboard from "@react-native-community/clipboard";
import { ChatMessage, ChatPerson, getName } from "app/chatUtils";
import { CHAT_MESSAGE_TYPE, LESSON_TYPE, LESSON_FILE_TYPE } from "app/constants";
import { diffTime } from "app/datetime";
import { checkContainLinkInText, checkIsOnlyEmojis } from "app/helper";
import IconBinRed from "assets/images/svg/IconBinRed.svg";
import IconCheckRead from "assets/images/svg/IconCheckRead.svg";
import IconCopy from "assets/images/svg/IconCopy.svg";
import IconOneCheck from "assets/images/svg/IconOneCheck.svg";
import {
  backgroundColor,
  fontRegular,
  grayColor,
  redColor,
  whiteColor,
} from "assets/styles/theme";
// components
// utils
// styles
import styles from "assets/styles/views/ChatDetail/component/chatContentStyle";
import stylesMarkdown from "assets/styles/views/ChatDetail/component/markDownStyles";
import GeneralAvatar from "components/GeneralAvatar/Avatar";
import moment from "moment";
import React, { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { LinkPreview } from "@flyerhq/react-native-link-preview";
import {
  FlatList,
  LayoutChangeEvent,
  Linking,
  Platform,
  Pressable,
  TouchableOpacity,
  View,
  Text,
} from "react-native";
import { Divider } from "react-native-elements";
import Markdown from "react-native-markdown-display";
import { useDispatch, useSelector } from "react-redux";
import { selectUsers, tapItemshareCollections } from "store/slices/chatSlice";
import { selectProfile } from "store/slices/userSlice";
import ChatShareAudio from "./ChatShareAudio";
import ChatShareFile from "./ChatShareFile";
import ChatShareImage from "./ChatShareImage";
import ChatShareRecord from "./ChatShareRecord";
import ChatShareScore from "./ChatShareScore";
import ChatShareVideo from "./ChatShareVideo";
import { EmptyView } from "./EmptyView";
import { PreviewLink } from "./PreviewLink";
import { formatDateTime } from "app/format";
import { shareDeepLink } from "store/slices/appSlice";
import ChatShareCollection from "./ChatShareCollection";

type ChatContentProps = {
  data: ChatMessage[];
  type: number;
  seen_at: number;
  refFlatList: React.RefObject<FlatList>;
  deleteMessage: (message: ChatMessage | undefined) => void;
};
const TIME_COMPARE = 120;

const FONT_SIZE_EMOIJ = Platform.select({
  ios: 48,
  android: 40,
});
const MARGIN_BOTTOM_EMOIJ = Platform.select({
  ios: -18,
  android: -11,
});
const MARGIN_TOP_EMOIJ = -8;
const LINE_HEIGHT_EMOIJ = Platform.select({
  ios: 54,
  android: 50,
});

const NameSender = ({
  sender_id,
  backgroundColor = "transparent",
}: {
  sender_id: string;
  backgroundColor?: string;
}) => {
  const users = useSelector(selectUsers);

  // const { first_name = "", last_name = "" } = users.find(
  //   (i: ChatPerson) => i.id === sender_id
  // );
  const name = users.find((i: ChatPerson) => i.id === sender_id);
  const colorName =
    name?.first_name && name?.first_name?.includes("T") ? "#5856D6" : "green";
  return (
    <View style={[styles.wrapperName, { backgroundColor }]}>
      <Text style={[styles.text, { color: colorName }]}>
        {getName(name?.first_name, name?.last_name)}
      </Text>
    </View>
  );
};
const ChatContent = (props: ChatContentProps) => {
  const { data, deleteMessage, refFlatList, type } = props;
  const { t,i18n } = useTranslation();
  const profile = useSelector(selectProfile);
  const users = useSelector(selectUsers);
  const dispatch =  useDispatch();
  const [layout, setLayout] = useState({
    height: 0,
    width: 0,
  });
  const [showActionMessage, setShowActionMessage] = React.useState<{
    show: boolean;
    item: ChatMessage | undefined;
  }>({ show: false, item: undefined });

  const chatText = (item: ChatMessage, index: number) => {
    const isMine = profile.id === item.sender_id;

    const isOnlyEmoij = checkIsOnlyEmojis(item.message?.trim());
    const borderRadiusStyle = genStyleWrapperMessage(
      profile.id,
      index,
      data,
      isOnlyEmoij
    );

    const isShowName = validateDisplayName(item, index, profile.id);

    const { isContain, text, links } = checkContainLinkInText(item?.message);

    const bgColor = isOnlyEmoij
      ? "transparent"
      : isMine
      ? "#F2F2F7"
      : whiteColor;

    return (
      <View
        style={[
          styles.wrapperText,
          borderRadiusStyle,
          { backgroundColor: bgColor },
          {
            paddingRight: isMine ? 25 : 10,
          },
        ]}
      >
        {!isOnlyEmoij && isShowName && (
          <NameSender sender_id={item.sender_id} />
        )}
        <Markdown
          mergeStyle
          onLinkPress={(url) => {
            // Linking.openURL(url);
            dispatch(shareDeepLink(url))
            return false;
          }}
          style={{
            ...stylesMarkdown,
            body: {
              fontFamily: fontRegular,
              fontSize: !isOnlyEmoij ? 16 : FONT_SIZE_EMOIJ,
              lineHeight: !isOnlyEmoij ? 21 : LINE_HEIGHT_EMOIJ,
              marginBottom: !isOnlyEmoij ? undefined : MARGIN_BOTTOM_EMOIJ,
              marginTop: !isOnlyEmoij ? undefined : MARGIN_TOP_EMOIJ,
            },
          }}
        >
          {isContain ? text : item?.message}
        </Markdown>
        {isContain && links && (
          <LinkPreview
            text={links[0]}
            renderText={() => null}
            renderLinkPreview={({ previewData }) => (
              <PreviewLink previewData={previewData} />
            )}
            containerStyle={styles.linkPreview}
          />
        )}
      </View>
    );
  };

  const validateDisplayName = useCallback(
    (item: ChatMessage, index: number, myId) => {
      if (type !== 1) {
        return false;
      }
      if (myId === item.sender_id) {
        return false;
      }
      if (item?.sender_id === data[index + 1]?.sender_id) {
        return false;
      }
      if (index === data.length - 1) {
        return true;
      }
      const { created_at, sender_id } = data[index + 1];

      if (sender_id !== item.sender_id) {
        return true;
      }
      const timeDistance = diffTime(item?.created_at, created_at);
      // && timeDistance > TIME_COMPARE
      if (sender_id === item.sender_id && timeDistance > TIME_COMPARE) {
        return true;
      }
      return false;
    },
    [data, type]
  );
  const renderItemChat = ({
    item,
    index,
  }: {
    item: ChatMessage;
    index: number;
  }) => {
    const isMine = profile.id === item.sender_id;
    const styleJustify = isMine ? "flex-end" : "flex-start";
    const { dateString, isShow } = getShowDate(item, index);
    return (
      <View>
        {isShow && (
          <Text
            style={{ textAlign: "center", color: grayColor[2], marginTop: 32 }}
          >
            {dateString}
          </Text>
        )}
        <View style={[styles.vRowcontainer, { justifyContent: styleJustify }]}>
          {renderWrapperItem(item, index, isShow)}
        </View>
      </View>
    );
  };
  const getShowDate = useCallback(
    (item: ChatMessage, index: number) => {
      let result: { isShow: boolean; dateString?: string } = {
        isShow: false,
        dateString: undefined,
      };
      const nextMessage = data[index + 1];
      const time = moment(item?.created_at);

      if (!nextMessage) {
        result.isShow = true;
      } else {
        const nextTime = moment(nextMessage.created_at);
        if (!time.isSame(nextTime, "days")) {
          result.isShow = true;
        } else {
          if (
            time.diff(nextTime, "minutes") >= TIME_COMPARE &&
            time.isSame(moment(), "days")
          ) {
            result.isShow = true;
          }
        }
      }

      if (result.isShow && !result.dateString) {
        result.dateString = formatDateTime(item?.created_at,i18n.language)
      }

      return result;
    },
    [data,i18n]
  );

  const validateDisplayAvatar = useCallback(
    (item: ChatMessage, index: number, myId) => {
      if (myId === item.sender_id) {
        return false;
      }
      if (item?.sender_id === data[index - 1]?.sender_id) {
        return false;
      }
      return true;
    },
    [data]
  );
  const renderAvatar = (item: ChatMessage, index: number) => {
    const isMine = profile.id === item.sender_id;

    const showWrapper = type === 1;

    const showAvatar = validateDisplayAvatar(item, index, profile?.id);
    if (!showWrapper || isMine) return null;

    // const {
    //   first_name = "",
    //   last_name = "",
    //   avatar,
    // } = users.find((i: ChatPerson) => i.id === item.sender_id);
    const name = users.find((i: ChatPerson) => i.id === item.sender_id);
    return (
      <View style={{ height: 40, width: 40, marginRight: 10 }}>
        {showAvatar && (
          <GeneralAvatar
            image={name?.avatar}
            size={40}
            text={getName(name?.first_name, name?.last_name)}
          />
        )}
      </View>
    );
  };

  const renderWrapperItem = (
    item: ChatMessage,
    index: number,
    isShowDate: boolean
  ) => {
    const isMine = profile.id === item.sender_id;
    const seen = item.created_at < props.seen_at;
    const removed = item.type === CHAT_MESSAGE_TYPE.Deleted;

    const showAction = () => setShowActionMessage({ show: true, item: item });
    const onPressItemShareCollection = (item:any) =>{
      const data ={
        chat_id:item?.chat_id,
        item_id:item?.item_id,
        conversation_id:item?.conversation_id,
      }
      //@ts-ignore
      dispatch(tapItemshareCollections(data))
    }
    const borderRadiusStyle = genStyleWrapperMessage(
      profile.id,
      index,
      data,
      isShowDate
    );

    return (
      <View style={styles.containerRowMessage}>
        {renderAvatar(item, index)}
        <View>
          <Pressable
            onPress={hideActionMessage}
            onLongPress={showAction}
            style={[
              !removed
                ? styles.wrapperMessage
                : { ...styles.removeMessage, ...borderRadiusStyle },
            ]}
          >
            {removed ? (
              <Text style={styles.txtRemoved}>{t("chat.attach.removed")}</Text>
            ) : (
              <>
                {renderContentChat(item, index, isShowDate, {
                  showAction,
                  hideActionMessage,
                  onPressItemShareCollection,
                })}
                {isMine && (
                  <View style={styles.wrapperSeenIcon}>
                    {item.id !== "LOADING_ID" &&
                      (seen ? <IconCheckRead /> : <IconOneCheck />)}
                  </View>
                )}
              </>
            )}
          </Pressable>
          {showActionMessage.item?.id === item.id &&
            !removed &&
            popupAction(isMine)}
        </View>
      </View>
    );
  };
  const popupAction = (isMine: boolean) => {
    return (
      <View style={styles.optionsContainer}>
        <TouchableOpacity
          style={styles.menuOption}
          onPress={() => actionMessage(1)}
        >
          <Text style={{ fontSize: 16,color:grayColor[2] }}>{t("chat.attach.copy")}</Text>
          <IconCopy />
        </TouchableOpacity>
        {isMine ? (
          <>
            <Divider />
            <TouchableOpacity
              style={styles.menuOption}
              onPress={() => actionMessage(2)}
            >
              <Text style={{ color: redColor[0], fontSize: 16 }}>
                {t("chat.attach.delete")}
              </Text>
              <IconBinRed />
            </TouchableOpacity>
          </>
        ) : null}
      </View>
    );
  };

  const actionMessage = (value: 1 | 2) => {
    hideActionMessage();
    value === 2
      ? deleteMessage(showActionMessage.item)
      : Clipboard.setString(
          showActionMessage.item?.message ? showActionMessage.item?.message : ""
        );
  };

  const hideActionMessage = () =>
    setShowActionMessage({ show: false, item: undefined });
  const renderContentChat = (
    item: ChatMessage,
    index: number,
    isShowDate: boolean,
    action: { showAction: () => void; hideActionMessage: () => void ;onPressItemShareCollection: (item:any) => void;}
  ) => {
    switch (item.type) {

      case CHAT_MESSAGE_TYPE.Text:
        return chatText(item, index);
      case CHAT_MESSAGE_TYPE.Record:
        return chatShareRecord(item, index, action, isShowDate);
      case CHAT_MESSAGE_TYPE.Score:
        return chatShareScore(item, index, action, isShowDate);
        case CHAT_MESSAGE_TYPE.UserScore:
          return chatShareScore(item, index, action, isShowDate);
      case CHAT_MESSAGE_TYPE.Image:
        return chatShareImage(item, index, action, isShowDate);
      case CHAT_MESSAGE_TYPE.Video:
        return chatShareVideo(item, index, action, isShowDate);
      case CHAT_MESSAGE_TYPE.Audio:
        return chatShareAudio(item, index,action,  isShowDate,);
      case CHAT_MESSAGE_TYPE.File:
        return chatShareFile(item, index, action, isShowDate);
      case CHAT_MESSAGE_TYPE.Lesson:
        const LessonType = JSON.parse(item.message).type;
        const LessonFileType = JSON.parse(item.message).file_type;
        if(LessonType === LESSON_TYPE.Score){
          return chatShareScore(item, index, action, isShowDate);
        }else if(LessonType === LESSON_TYPE.File){
          if(LessonFileType === LESSON_FILE_TYPE.AUDIO){
            return chatShareAudio(item, index,action, isShowDate,);
          }else{
            return chatShareFile(item, index, action, isShowDate);
          }
        }
        case CHAT_MESSAGE_TYPE.LessonPlan:
          return chatShareCollection(item, index, action, isShowDate);
      default:
        return <View />;
    }
  };


  const  chatShareCollection = (
    item: ChatMessage,
    index: number,
    action: { showAction: () => void; hideActionMessage: () => void;onPressItemShareCollection:(item:any)=>void },
    isShowDate: boolean
  ) => {
    const borderRadiusStyle = genStyleWrapperMessage(
      profile.id,
      index,
      data,
      isShowDate
    );

    return (
      <View style={{ ...borderRadiusStyle, overflow: "hidden" }}>
        <ChatShareCollection item={item} action={action}/>
      </View>
    );
  }

  const chatShareFile = (
    item: ChatMessage,
    index: number,
    action: { showAction: () => void; hideActionMessage: () => void;onPressItemShareCollection:(item:any)=>void },
    isShowDate: boolean
  ) => {
    const borderRadiusStyle = genStyleWrapperMessage(
      profile.id,
      index,
      data,
      isShowDate
    );

    return (
      <View style={{ ...borderRadiusStyle, overflow: "hidden" }}>
        <ChatShareFile item={item} action={action} />
      </View>
    );
  };

  const chatShareAudio = (
    item: ChatMessage,
    index: number,
    action: { onPressItemShareCollection:(item:any)=>void},
    isShowDate: boolean,
  ) => {
    const borderRadiusStyle = genStyleWrapperMessage(
      profile.id,
      index,
      data,
      isShowDate
    );

    return (
      <View style={{ ...borderRadiusStyle, overflow: "hidden" }}>
        <ChatShareAudio item={item} action={action}/>
      </View>
    );
  };
  const chatShareVideo = (
    item: ChatMessage,
    index: number,
    action: { showAction: () => void; hideActionMessage: () => void },
    isShowDate: boolean
  ) => {
    const borderRadiusStyle = genStyleWrapperMessage(
      profile.id,
      index,
      data,
      isShowDate
    );

    return (
      <View style={{ ...borderRadiusStyle, overflow: "hidden" }}>
        <ChatShareVideo item={item} action={action} />
      </View>
    );
  };
  const chatShareImage = (
    item: ChatMessage,
    index: number,
    action: { showAction: () => void; hideActionMessage: () => void },
    isShowDate: boolean
  ) => {
    const borderRadiusStyle = genStyleWrapperMessage(
      profile.id,
      index,
      data,
      isShowDate
    );
    return (
      <View style={{ ...borderRadiusStyle, overflow: "hidden" }}>
        <ChatShareImage item={item} action={action} />
      </View>
    );
  };
  const chatShareRecord = (
    item: ChatMessage,
    index: number,
    action: { showAction: () => void; hideActionMessage: () => void },
    isShowDate: boolean
  ) => {
    const borderRadiusStyle = genStyleWrapperMessage(
      profile.id,
      index,
      data,
      isShowDate
    );
    const showName = validateDisplayName(item, index, profile?.id);
    return (
      <View
        style={[borderRadiusStyle, !showName && styles.borderRecordScoreCard]}
      >
        <View
          style={{ ...borderRadiusStyle, overflow: "hidden", marginTop: 0 }}
        >
          {showName && (
            <>
              <NameSender
                sender_id={item.sender_id}
                backgroundColor={whiteColor}
              />
              <View style={styles.lineHorizontal} />
            </>
          )}
          <ChatShareRecord item={item} action={action} />
        </View>
      </View>
    );
  };

  const chatShareScore = (
    item: ChatMessage,
    index: number,
    action: { showAction: () => void; hideActionMessage: () => void;onPressItemShareCollection:(item:any)=>void  },
    isShowDate: boolean,
  ) => {
    const borderRadiusStyle = genStyleWrapperMessage(
      profile.id,
      index,
      data,
      isShowDate
    );
    const showName = validateDisplayName(item, index, profile?.id);

    return (
      <View
        style={[borderRadiusStyle, !showName && styles.borderRecordScoreCard]}
      >
        <View
          style={{ ...borderRadiusStyle, overflow: "hidden", marginTop: 0 }}
        >
          {showName && (
            <>
              <NameSender
                sender_id={item.sender_id}
                backgroundColor={whiteColor}
              />
              <View style={styles.lineHorizontal} />
            </>
          )}
          <ChatShareScore item={item} action={action}/>
        </View>
      </View>
    );
  };

  return (
    <FlatList
      ref={refFlatList}
      onScrollEndDrag={() => showActionMessage.show && hideActionMessage()}
      style={styles.container}
      data={data}
      inverted
      renderItem={renderItemChat}
      onLayout={({
        nativeEvent: {
          layout: { height, width },
        },
      }: LayoutChangeEvent) => {
        setLayout({ height, width });
      }}
      contentContainerStyle={styles.contentContainer}
      keyExtractor={(item) => item.created_at.toString()}
      ListEmptyComponent={() => (
        <EmptyView height={layout.height} width={layout.width} />
      )}
    />
  );
};
export default ChatContent;
const genStyleWrapperMessage = (
  myId: string,
  index: number,
  data: ChatMessage[],
  isShowDate: boolean,
  isOnlyEmoij?: boolean
) => {
  const isMine = data[index].sender_id === myId;
  let wrapperStyle = {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    marginTop: isOnlyEmoij ? 0 : 8,
  };
  const afterIsSame = data[index + 1]?.sender_id === data[index].sender_id;
  const beforeIsSame = data[index - 1]?.sender_id === data[index].sender_id;
  if (isMine) {
    if (!beforeIsSame) {
      wrapperStyle.borderBottomRightRadius = 0;
      wrapperStyle.borderBottomLeftRadius = 20;
    }
    if (!afterIsSame) {
      wrapperStyle.borderTopRightRadius = 20;
      wrapperStyle.borderTopLeftRadius = 20;
      if (!isShowDate) {
        wrapperStyle.marginTop = 32;
      }
    }
  } else {
    if (!afterIsSame) {
      wrapperStyle.borderTopRightRadius = 20;
      wrapperStyle.borderTopLeftRadius = 20;
      if (!isShowDate) {
        wrapperStyle.marginTop = 32;
      }
    }
    if (!beforeIsSame) {
      wrapperStyle.borderBottomRightRadius = 20;
      wrapperStyle.borderBottomLeftRadius = 0;
    }
  }

  return wrapperStyle;
};
