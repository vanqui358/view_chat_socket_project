import { useNavigation } from "@react-navigation/native";
import {
  blackColor,
  blueColor,
  fontBold,
  fontRegular,
  grayColor,
  redColor,
} from "assets/styles/theme";
import React from "react";
import { useTranslation } from "react-i18next";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Animated, {
  useAnimatedStyle,
  useDerivedValue,
  withTiming,
} from "react-native-reanimated";
import { useDispatch, useSelector } from "react-redux";
import {
  handleInvitationGroup,
  selectDataGroupDetail,
  selectContacts,
  selectUserById,
  selectUsers,
} from "store/slices/chatSlice";
interface Props {
  type: number;
}
export const GroupInvitation = ({ type }: Props) => {
  const navigation = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const group = useSelector(selectDataGroupDetail);
  const contacts = useSelector(selectContacts);

  const isShow = type === 1 && group?.invite_status === 0;
  const heightAni = useDerivedValue(() =>
    isShow ? withTiming(64) : withTiming(0)
  );
  React.useEffect(() => {});
  // const users = useSelector(selectUsers);
  // const name = users.find((c: any) => c.id === group?.invited_by?.id);

  // const nameInvited = name
  //   ? (name?.first_name === null ? "" : name?.first_name) +" "+
  //     (name?.last_name === null ? "" : name?.last_name)
  //   : "Noname";
  const containerStyleAni = useAnimatedStyle(() => ({
    height: heightAni.value,
  }));
  const onAcceptPress = () => {
    //@ts-ignore
    dispatch(handleInvitationGroup({ groupId: group?.id, status: 1 }));
  };
  const onRejectPress = () => {
    //@ts-ignore
    dispatch(handleInvitationGroup({ groupId: group?.id, status: 2 }));
    navigation.goBack();
  };
  return (
    <Animated.View
      style={[styles.container, containerStyleAni]}
      pointerEvents={isShow ? "auto" : "none"}
    >
      <View style={{ flex: 1 }}>
        <Text style={styles.txtName}>{group?.invited_by?.name}</Text>
        <Text style={styles.txtSubTitle}>
          {t("chat_history.invite_you_to_group", {
            name: "",
          })}
        </Text>
      </View>
      <TouchableOpacity onPress={onRejectPress}>
        <Text style={[styles.txtBtn, { color: redColor[0] }]}>
          {t("label.reject")}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={onAcceptPress}>
        <Text style={[styles.txtBtn, { color: blueColor[0] }]}>
          {t("label.accept")}
        </Text>
      </TouchableOpacity>
    </Animated.View>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#F2F2F7",
    borderTopColor: grayColor[3],
    borderTopWidth: 1,
    paddingHorizontal: 16,
  },
  txtBtn: { fontFamily: fontRegular, fontSize: 16, marginLeft: 30 },
  txtName: { fontFamily: fontBold, fontSize: 18, color: blackColor },
  txtSubTitle: { fontFamily: fontRegular, fontSize: 18, color: blackColor },
});
