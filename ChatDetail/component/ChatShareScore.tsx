import { useNavigation } from "@react-navigation/native";
import { ChatMessage } from "app/chatUtils";
import { ScoreLevelColor } from "assets/styles/theme";
import styles from "assets/styles/views/ChatDetail/component/chatShareScoreStyle";
import React from "react";
import { useTranslation } from "react-i18next";
import { ActivityIndicator, Pressable, View } from "react-native";
import { Text } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import {  SCREENS } from "app/constants";
import { selectProfile } from "store/slices/userSlice";
import { useScoreInfoStaticData } from "hooks/data/scores/scoresDataHook";
import { getIcons } from "app/icons";
import { selectDataTapShare } from "store/slices/chatSlice";
import { sendRequest } from "store/slices/appSlice";
import { apiTapItemShareCollections } from "api/shareApi";

type ScreenProps = {
  item: ChatMessage;
  action: { showAction: () => void; hideActionMessage: () => void;onPressItemShareCollection:(item:any)=>void };
  type?:number,
};

type SharedScoreMessageType = {
  id: number;
  name: string;
  ref_id: number;
  score_id: number;
  shared_items?: { [id: string]: string };
};

const IconScoreInChat = getIcons("IconScoreInChat");

const ChatShareScore = (props: ScreenProps) => {
  const { t } = useTranslation();
  // const dispatch = useDispatch();
  const navigation = useNavigation();
  const scoreInfo = React.useMemo<SharedScoreMessageType>(
    () => JSON.parse(props.item.message),
    [props.item.message]
  );
  const scoreId = scoreInfo.score_id || props.item.ref_id;

  // useSelector
  const score = useScoreInfoStaticData(scoreId);
  const profile = useSelector(selectProfile);
  const dispatch = useDispatch();
  const instanceId = React.useMemo<string | null>(() => {
    if (!scoreInfo || !profile || !scoreInfo.shared_items) return null;
    return scoreInfo.shared_items[profile.id];
  }, [profile, scoreInfo]);

  const onPress = React.useCallback(async () => {
    const item = {
      chat_id:props.item?.id,
      item_id:props.item.ref_id,
      conversation_id:props.item?.conversation_id,
    }
    // props.action.onPressItemShareCollection(item);
    const response = await sendRequest(
      dispatch,
      async (accessToken:any) => await apiTapItemShareCollections(accessToken,item)
    );
    if (!scoreInfo) return;
    //@ts-ignore   
    navigation.navigate(SCREENS.ScoreDetail, {
      id: scoreId,
      title: scoreInfo.name,
      instanceId: response?.data?.item_id,
    });
    props.action.hideActionMessage();
  }, [props.item?.id, props.item.ref_id, props.item?.conversation_id, props.action, dispatch, scoreInfo, navigation, scoreId]);

  

  const levelBgStyle = React.useMemo(() => {
    let color = ScoreLevelColor[0];
    if (score) {
      color =
        ScoreLevelColor[
          score.level > 0 && score.level <= 12 ? score.level - 1 : 0
        ];
    }
    return {
      backgroundColor: color,
    };
  }, [score]);

  const isMine = profile.id === props.item.sender_id;

  return (
    <Pressable
      onPress={onPress}
      onLongPress={props.action.showAction}
      style={[
        styles.container,
        {
          paddingRight: isMine ? 25 : 10,
        },
      ]}
    >
      {!score && <ActivityIndicator size={32} color={"white"} />}
      {score && (
        <>
          <View style={styles.iconScore}>
            <IconScoreInChat with={15} height={20} />
          </View>
          <View style={styles.wrapperTextInfo}>
            <Text style={styles.txtTitle}>{score?.name}</Text>
            <Text style={styles.txtNameAuthor}>{score?.composer}</Text>
          </View>
          <View style={[styles.wrapperLevel, levelBgStyle]}>
            <Text style={styles.txtLevel}>
              {" "}
              {t("label.score_level", { level: score?.level }).toUpperCase()}
            </Text>
          </View>
        </>
      )}
    </Pressable>
  );
};
export default ChatShareScore;
