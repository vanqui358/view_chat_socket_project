import { WSS_ROOT } from "config";
import io, { Socket } from "socket.io-client";
import { msTimestamp } from "app/datetime";

const LOG_ENABLE = false;
const REQUEST_TIMEOUT = 10000;
const PING_INTERVAL = 20000;
const MAX_RECONNECT_DELAY = 60000;

export type ResponseData = {
  code: number;
  message: string;
  data: Object | null;
};

export type ResponseFunction = {
  (data: ResponseData): void;
};

export type EventFunction = {
  (data: Object | null): void;
};

type Request = {
  id: string;
  key: string;
  start: number;
  onResponse: ResponseFunction;
};

type Event = {
  key: string;
  onEvent: EventFunction;
};

type OnResponseData = {
  code: number;
  message: string;
  data: Object | null;
  _id: string | null | undefined;
};

const ResDataNetwordError = {
  code: 99,
  message: "error.socket_error",
  data: null,
};

const ResDataNetwordTimeout = {
  code: -2,
  message: "error.socket_timeout",
  data: null,
};

export class WsClient {
  private io: Socket | null = null;
  private accessToken: string | null = null;
  private reqCount: number = 0;
  private running: boolean = false;
  private lastPing: number = 0;
  private requests: {
    [key: string]: {
      handler: { (data: OnResponseData): void };
      items: Request[];
    };
  } = {};
  private events: {
    [key: string]: {
      handler: { (data: Object | null): void };
      items: Event[];
    };
  } = {};

  private log(message: any): void {
    if (LOG_ENABLE) console.log(`Socket: ${message}`);
  }

  public start(authToken: string): void {
    if (this.running) return;
    this.log("Start!");
    this.running = true;
    this.accessToken = authToken;
    this.lastPing = 0;
    if (this.io === null) {
      this.log("Create New socket");
      this.io = io(WSS_ROOT, {
        reconnectionDelayMax: MAX_RECONNECT_DELAY,
        extraHeaders: {
          Authorization: `Bearer ${this.accessToken}`,
        },
        transports: ["websocket"],
        reconnection: true,
        forceNew: true,
      });
      this.onEvent("connect", () => this.onConnect());
      this.onEvent("disconnect", (reason) => this.onDisconnect(reason));
      // Register callbacks
      Object.keys(this.requests).forEach((k) => {
        this.io?.on(k, this.requests[k].handler);
      });
      Object.keys(this.events).forEach((k) => {
        this.io?.on(k, this.events[k].handler);
      });
    } else {
      this.io.io.opts.extraHeaders = {
        Authorization: `Bearer ${this.accessToken}`,
      };
      this.io.connect();
    }
  }

  public stop(): void {
    if (!this.running) return;
    this.log("Stop!");
    this.running = false;
    this.io?.disconnect();
  }

  public update(): void {
    if (!this.isConnected()) return;
    const now = msTimestamp();
    // ping
    if (this.lastPing + PING_INTERVAL < now) {
      // skip first ping
      if (this.lastPing > 0) {
        this.request("ping", {});
      }
      this.lastPing = now;
    }
    // check timeout requests
    const timeoutRequests: Request[] = [];
    Object.keys(this.requests).forEach((k) => {
      this.requests[k].items.forEach((r) => {
        if (r.start + REQUEST_TIMEOUT < now) {
          timeoutRequests.push(r);
        }
      });
    });
    timeoutRequests.forEach((r) =>
      this.onResponse(r.key, {
        _id: r.id,
        ...ResDataNetwordTimeout,
      })
    );
  }

  private onConnect(): void {
    this.log("Connected!");
  }

  private onDisconnect(reason: any): void {
    this.log("Disconnected! " + reason);
    // // Clean up callbacks
    // Response error to pending requests
    Object.keys(this.requests).forEach((k) => {
      const rIds = this.requests[k].items.map((r) => r.id);
      rIds.forEach((rid) =>
        this.onResponse(k, {
          _id: rid,
          ...ResDataNetwordError,
        })
      );
    });
    if (this.io) {
      this.io.sendBuffer = [];
      if (reason === "io server disconnect") {
        this.io.connect();
      }
    }
  }

  private onResponse(key: string, data: OnResponseData): void {
    const reqId = data._id;
    this.log(`onResponse "${key}" [${reqId}] | message: ${data.message}`);
    const idx = this.requests[key].items.findIndex((e) => e.id === reqId);
    let request = null;
    if (idx > -1) {
      request = this.requests[key].items[idx];
      this.requests[key].items.splice(idx, 1);
    } else {
      request = this.requests[key].items.shift();
    }
    if (request) {
      try {
        request.onResponse({
          code: data.code,
          message: data.message,
          data: data.data,
        });
      } catch (e) {
        console.error(e);
      }
    }
  }

  private registerRequest(key: string): void {
    if (!this.requests[key]) {
      this.requests[key] = {
        handler: (data: OnResponseData) => this.onResponse(key, data),
        items: [],
      };
      this.io?.on(key, this.requests[key].handler);
    }
  }

  private registerEvent(key: string): void {
    if (!this.events[key]) {
      this.events[key] = {
        handler: (data: Object | null) => {
          this.log(
            `onEvent ${key} | ${this.events[key].items.length} | ${
              !data ? "" : JSON.stringify(data)
            }`
          );
          this.events[key].items.forEach((event) => {
            try {
              event.onEvent(data);
            } catch (e) {
              console.error(e);
            }
          });
        },
        items: [],
      };
      this.io?.on(key, this.events[key].handler);
    }
  }

  public request(key: string, reqData: Object | null): Promise<ResponseData> {
    return new Promise((resolve) => {
      this.registerRequest(key);
      this.reqCount++;
      const reqId = this.reqCount.toString();
      this.requests[key].items.push({
        id: reqId,
        key: key,
        start: msTimestamp(),
        onResponse: (resData) => resolve(resData),
      });
      const sentData = {
        _id: reqId,
        ...reqData,
      };
      this.log(`Emit: ${key} | ${JSON.stringify(sentData)}`);
      this.io?.emit(key, sentData);
    });
  }

  public onEvent(key: string, callback: EventFunction): void {
    // Check init event key
    this.registerEvent(key);
    // If the callback hasn't been registered, add it
    const idx = this.events[key].items.findIndex((e) => e.onEvent === callback);
    if (idx < 0) {
      this.events[key].items.push({
        key: key,
        onEvent: callback,
      });
      this.log(`Add event: ${key}`);
    }
  }

  public offEvent(key: string, callback: EventFunction): void {
    if (!this.events[key]) return;
    const idx = this.events[key].items.findIndex((e) => e.onEvent === callback);
    if (idx >= 0) {
      this.events[key].items.splice(idx, 1);
      this.log(`Remove event: ${key}`);
    }
  }

  public isConnected(): boolean {
    if (!this.io) return false;
    return this.io.connected;
  }
}

const Ws = new WsClient();
export default Ws;
