import Ws, { EventFunction } from "./Ws";

export async function wsFetchConversations() {
  return await Ws.request("chat/fetch_conversations", {});
}

export async function wsFetchUsers() {
  return await Ws.request("chat/fetch_users", {});
}

export async function wsFetchMessages(conversationId: string) {
  return await Ws.request("chat/fetch_messages", {
    conversation_id: conversationId,
  });
}

export async function wsSendMessage(
  conversationId: string,
  type: number,
  message: string
) {
  return await Ws.request("chat/send_message", {
    conversation_id: conversationId,
    type: type,
    message: message,
  });
}

export async function wsDeleteMessage(
  conversationId: string,
  messageId: string
) {
  return await Ws.request("chat/delete_message", {
    conversation_id: conversationId,
    message_id: messageId,
  });
}

export async function wsReadConversation(conversationId: string) {
  return await Ws.request("chat/read_conversation", {
    conversation_id: conversationId,
  });
}

export async function wsUnreadConversation(conversationId: string) {
  return await Ws.request("chat/unread_conversation", {
    conversation_id: conversationId,
  });
}

export async function wsPinConversation(conversationId: string) {
  return await Ws.request("chat/pin_conversation", {
    conversation_id: conversationId,
  });
}

export async function wsUnpinConversation(conversationId: string) {
  return await Ws.request("chat/unpin_conversation", {
    conversation_id: conversationId,
  });
}

export async function wsMuteConversation(conversationId: string) {
  return await Ws.request("chat/mute_conversation", {
    conversation_id: conversationId,
  });
}

export async function wsUnmuteConversation(conversationId: string) {
  return await Ws.request("chat/unmute_conversation", {
    conversation_id: conversationId,
  });
}

export async function wsArchiveConversation(conversationId: string) {
  return await Ws.request("chat/archive_conversation", {
    conversation_id: conversationId,
  });
}

export async function wsUnarchiveConversation(conversationId: string) {
  return await Ws.request("chat/unarchive_conversation", {
    conversation_id: conversationId,
  });
}

export async function wsFetchNotificationFeatures() {
  return await Ws.request("notification/fetch_status", {});
}

export async function wsSeenFeature(feature: string) {
  return await Ws.request("notification/set_seen", {
    type: feature,
  });
}

export function wsOnAddConversation(handler: EventFunction) {
  Ws.onEvent("chat/on_add_conversation", handler);
}

export function wsOnUpdateConversation(handler: EventFunction) {
  Ws.onEvent("chat/on_update_conversation", handler);
}

export function wsOnDeleteConversation(handler: EventFunction) {
  Ws.onEvent("chat/on_delete_conversation", handler);
}

export function wsOnUpsertUser(handler: EventFunction) {
  Ws.onEvent("chat/on_upsert_user", handler);
}

export function wsOnDeleteUser(handler: EventFunction) {
  Ws.onEvent("chat/on_delete_user", handler);
}

export function wsOnAddMessage(handler: EventFunction) {
  Ws.onEvent("chat/on_add_message", handler);
}

export function wsOnUpdateMessage(handler: EventFunction) {
  Ws.onEvent("chat/on_update_message", handler);
}

export function wsOnUpdateNotificationFeature(handler: EventFunction) {
  Ws.onEvent("notification/on_update", handler);
}
