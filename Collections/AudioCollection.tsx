import React from 'react';
import {View,Text, TouchableOpacity,StyleSheet,Dimensions} from 'react-native';
import { usePlayAudio } from 'hooks/components/Audio/playbackAudioControllerHook';
import { formatDuration } from "app/format";
import { getIcons } from 'app/icons';
import { grayColor, whiteColor } from "assets/styles/theme";
import {AudioPlayer} from 'react-native-simple-audio-player';


interface AudioCollectionProps {
    url:string,
}

const BtnPlay = getIcons("btnPlay");
const BtnPause = getIcons("btnPause");
const deviceWidth = Dimensions.get("window").width;

const AudioCollection  = (props:AudioCollectionProps) => {
const {url} = props;
  const { msDuration, isPlaying, onPlayPress, currentTime } = usePlayAudio({
    file: encodeURI(url),
  });

    return(
    //   <View style={{flex:1,backgroundColor:"#333",justifyContent:"flex-end"}}>
    //     <View  style={[styles.container]}>
    //         <TouchableOpacity onPress={onPlayPress}>
    //         <Icon width={40} height={40} />
    //       </TouchableOpacity>
    //     <View style={styles.wrapperTitle}>
    //     <Text numberOfLines={1} style={styles.title}>
    //       {arrElementUrl[arrElementUrl.length - 1]}
    //     </Text>
    //     <Text style={styles.currentTime}>{formatDuration(currentTime)}</Text>
    //   </View>
    //   <Text style={styles.duration}>{formatDuration(msDuration / 1000)}</Text>
    // </View>
    // </View>

    <View
    style={{
      flex: 1,
      backgroundColor: '#313131',
      justifyContent: 'flex-end',
    }}>
    <AudioPlayer
      url={encodeURI(url)}
    />
  </View>

    );
};

export default AudioCollection;


const styles = StyleSheet.create({
  container: {
    padding: 10,
    
    backgroundColor: whiteColor,
    alignItems: "center",

  },
  wrapperContent: { flexDirection: "row", alignItems: "center", },
  wrapperTitle: {marginHorizontal: 10,flexDirection:"row",justifyContent:"space-around",backgroundColor:"#444",width:deviceWidth},
  title: { fontWeight: "700", fontSize: 16, color: grayColor[2] },
  currentTime: { color: grayColor[3] },
  duration: {
    alignSelf: "flex-end",
    fontWeight: "500",
    fontSize: 15,
    color: grayColor[2],
  },
  indicator: {
    height: 40,
    width: 40,
  },
});


