/* eslint-disable no-undef */
import React from "react";
import { View, Text, Dimensions } from "react-native";
import { useTheme } from "react-native-paper";
import ContentCard from "components/Card/ContentCard";
import FastImage from "react-native-fast-image";
// images
import IconTagGuitar from "assets/images/svg/IconTagGuitar.svg";
import IconTagPiano from "assets/images/svg/IconTagPiano.svg";
import IconTagSaxo from "assets/images/svg/IconTagSaxo.svg";
import IconTagViolin from "assets/images/svg/IconTagViolin.svg";
import IconTagDanbau from "assets/images/svg/IconTagDanbau.svg";
import { useTranslation } from "react-i18next";
import makeStyles from "assets/styles/components/Card/scoreCardStyles";
import { ScoreLevelColor } from "assets/styles/theme";
import CardActionButton from "components/Button/CardActionButton";
import Video from "react-native-video";
import PDFView from "react-native-view-pdf";
import { getIcons } from "app/icons";
import AudioCollection from "./AudioCollection";
import { useIAP } from "react-native-iap";
import { SCREENS } from "app/constants";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import Icon from "react-native-vector-icons/FontAwesome5";
import YoutubeInline from "components/Inline/YoutubeInline";

interface addFile {
  item?: any;
  onTapItemCollection?: (file: string | null) => void;
  style?: any;
  favourite: boolean;
  instanceId: string | null;
  attachable: number;
  onCardPress: (
    id: number,
    title: string,
    composer: string,
    instanceId: string | null
  ) => void;
  onFavoriteCollection: (params: number, isFavorite: boolean) => void;
  onAttachPressed: (id: number, user_score_id: string | null) => void;
  onShareCollectionPressed: (id: number, instanceId: string | null) => void;
  onAddScoreColectionPress: (id: number, instanceId: string | null) => void;
  onCommentPress: (id: number, instanceId: string | null) => void;
}
const BtnPlay = getIcons("btnPlay");
const deviceWidth = Dimensions.get("window").width;
const maxWidth = deviceWidth;

const ScreenPreviewCardCollection = (props: addFile) => {
  const TYPE_FILE = {
    IMAGE: 0,
    VIDEO: 1,
    PDF: 2,
    AUDIO: 3,
  };
  const {
    item,
    onTapItemCollection,
    style,
    onCardPress,
    instanceId,
    favourite,
    attachable,
    onCommentPress,
    onFavoriteCollection,
    onAttachPressed,
    onShareCollectionPressed,
  } = props;
  const [favorited, setFavorited] = React.useState<boolean>(item.favorite);
  const [playingYoutubeId, setPlayingYoutubeId] = React.useState(null);
  const { currentPurchase } = useIAP();
  const navigation = useNavigation();
  const level = item?.score?.level;
  const { t } = useTranslation();
  const theme = useTheme();
  const styles = React.useMemo<any>(() => makeStyles(theme), [theme]);
  const onScreenFocus = React.useCallback(() => {
    setPlayingYoutubeId(null);
    return () => {
      setPlayingYoutubeId(null);
    };
  }, []);
  useFocusEffect(onScreenFocus);
  const youtubeId = React.useMemo<string | null>(
    () =>
      item?.type === 1 && item?.file_type === 1
        ? item?.file?.substring("https://youtu.be/".length) || null
        : null,
    [item]
  );

  const levelBgStyle = React.useMemo(
    () => ({
      backgroundColor:
        ScoreLevelColor[level > 0 && level <= 12 ? level - 1 : 0],
    }),
    [level]
  );

  const { canAttach, icAttach } = React.useMemo(() => {
    let canAttach = false;
    let icAttach: "attach" | "attached" | "attachedMini" | "noAttach" =
      "noAttach";
    switch (attachable) {
      case 1:
        icAttach = "attach";
        canAttach = true;
        break;
      case 2:
        icAttach = "attached";
        canAttach = true;
        break;
      default:
        icAttach = "noAttach";
        break;
    }
    return {
      canAttach,
      icAttach,
    };
  }, [attachable]);


  const onCardPressedCallback = React.useCallback(() => {
    if (item?.subscription_status === 1) {
      if (currentPurchase !== undefined) {
        if (item?.type === 3 || item?.type === 1) {
          onTapItemCollection(item?.type, item?.file, item?.file_type);
        } else {
          onCardPress &&
            onCardPress(
              item?.score?.id,
              item?.name,
              item?.score?.composer,
              instanceId
            );
        }
      } else {
        navigation.navigate(SCREENS.PremiumSubscription, { type: "premium" });
      }
    } else {
      if (item?.type === 3 || item?.type === 1) {
        onTapItemCollection(item?.type, item?.file, item?.file_type);
      } else {
        onCardPress &&
          onCardPress(
            item?.score?.id,
            item?.name,
            item?.score?.composer,
            instanceId
          );
      }
    }
  }, [
    instanceId,
    item,
    currentPurchase,
    onTapItemCollection,
    onCardPress,
    navigation,
  ]);

  const onFavoriteCollectionOnPress = (id: string) => {
    const params = {
      collection_id: id,
      data: { type: 3 },
    };
    setFavorited((v) => !v);
    onFavoriteCollection && onFavoriteCollection(params, !favorited);
  };

  const onBtnSharePressedCallback = React.useCallback(() => {
    if (item?.subscription_status === 1 && currentPurchase === undefined) {
      navigation.navigate(SCREENS.PremiumSubscription, { type: "premium" });
    } else {
      onShareCollectionPressed &&
        onShareCollectionPressed(item?.id, instanceId);
    }
  }, [
    item?.subscription_status,
    item?.id,
    currentPurchase,
    navigation,
    onShareCollectionPressed,
    instanceId,
  ]);

  const onBtnAttachPressedCallback = React.useCallback(() => {
    if (!canAttach) return;
    onAttachPressed && onAttachPressed(item?.id, instanceId);
  }, [canAttach, onAttachPressed, item?.id, instanceId]);

  const onBtnCommentPressedCallback = React.useCallback(() => {
    onCommentPress && onCommentPress(item?.id, instanceId);
  }, [item?.id, onCommentPress, instanceId]);

  const renderConver = (props: { style: any }) => {
    if (item?.type === 0) {
      return (
        <FastImage
          source={{
            uri: item?.score?.cover?.url,
            priority: FastImage.priority.normal,
          }}
          resizeMode={FastImage.resizeMode.contain}
          style={props.style}
        />
      );
    } else if (item?.type === 1) {
      if (item?.file_type === 0) {
        return (
          // eslint-disable-next-line react-native/no-inline-styles
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Video
              source={{ uri: encodeURI(item?.file) }}
              resizeMode="cover"
              muted
              rate={0.0}
              style={{ width: maxWidth, height: 180 }}
            />
            <View style={{ position: "absolute" }} pointerEvents="none">
              <BtnPlay />
            </View>
          </View>
        );
      } else {
        return (
          <YoutubeInline
            youtubeId={youtubeId}
            playingYoutubeId={playingYoutubeId}
            setPlayingYoutubeId={
              item?.subscription_status === 1
                ?currentPurchase === undefined?  () => {
                        navigation.navigate(SCREENS.PremiumSubscription, {
                            type: "premium",
                          });

                  }
                  :setPlayingYoutubeId
                : setPlayingYoutubeId
            }
          />
        );
      }
    } else {
      switch (item?.file_type) {
        case TYPE_FILE.IMAGE:
          return (
            <FastImage
              source={{
                uri: item?.file,
                priority: FastImage.priority.normal,
              }}
              style={props.style}
            />
          );
        case TYPE_FILE.VIDEO:
          return (
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Video
                source={{ uri: encodeURI(item?.file) }}
                resizeMode="cover"
                muted
                rate={0.0}
                style={{ width: maxWidth, height: 180 }}
              />
              <View style={{ position: "absolute" }} pointerEvents="none">
                <BtnPlay />
              </View>
            </View>
          );
        case TYPE_FILE.PDF:
          return (
            <PDFView
              resource={item?.file}
              style={props.style}
              resourceType={"url"}
              onError={(error) => {
                //   setUrl(globalUrl);
                console.log("Cannot render PDF", error);
              }}
            />
          );
        case TYPE_FILE.AUDIO:
          return <AudioCollection url={item?.file} />;

        default:
          return null;
      }
    }
  };
  return (
    <>
      <ContentCard
        title={item?.name}
        Cover={(props: { style: any }) => renderConver(props)}
        subtitle={item?.score?.composer || item?.composer || ""}
        preview={true}
        // () => onTapItemCollection(item?.file, item?.file_type)
        onCardPress={() =>
          item?.type === 1 && item?.file_type === 1
            ? null
            : onCardPressedCallback()
        }
        style={style}
        InfoLine1={(props: { style: any }) => (
          <View style={props.style}>
            <View style={styles.instrumentsWrapper}>
              {item?.subscription_status === 1 ? (
                <Icon name="crown" size={16} color={"#FFCC00"} />
              ) : null}
              {item?.score?.instruments.map((instrument) => {
                let Icon = null;
                switch (instrument) {
                  case 1:
                    Icon = IconTagPiano;
                    break;
                  case 2:
                    Icon = IconTagGuitar;
                    break;
                  case 3:
                    Icon = IconTagSaxo;
                    break;
                  case 4:
                    Icon = IconTagViolin;
                    break;
                  case 5:
                    Icon = IconTagDanbau;
                    break;
                  default:
                    return <></>;
                }
                return <Icon key={instrument} style={styles.iconInstrument} />;
              })}
            </View>
          </View>
        )}
        InfoLine2={(props: { style: any }) => (
          <View style={props.style}>
            {item?.type === 0 && (
              <View style={[styles.levelWrapper, levelBgStyle]}>
                <Text style={[styles.levelLabel]}>
                  {t("label.score_level", { level })}
                </Text>
              </View>
            )}
          </View>
        )}
        BottomActionsLeft={(props: { style: any }) => (
          <View style={props.style}>
            <CardActionButton
              icon={favorited ? "favorited" : "favourite"}
              onPress={() => onFavoriteCollectionOnPress(item?.id)}
            />
          </View>
        )}
        BottomActionsCenter={(props: { style: any }) => (
          <View style={props.style}>
            {item?.type === 0 && (
              <CardActionButton
                icon={"comment"}
                onPress={onBtnCommentPressedCallback}
                disabled={item?.score?.notation_count === null}
              />
            )}
          </View>
        )}
        BottomActionsRight={(props: { style: any }) => (
          <View style={props.style}>
            {item?.type === 0 && (
              <CardActionButton
                icon={icAttach}
                onPress={onBtnAttachPressedCallback}
              />
            )}
            <CardActionButton
              icon={"share"}
              onPress={onBtnSharePressedCallback}
            />
          </View>
        )}
      />
    </>
  );
};
export default ScreenPreviewCardCollection;
