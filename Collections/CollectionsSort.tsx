/* eslint-disable no-undef */
import React, { useContext } from "react";
import { View, Dimensions, useWindowDimensions, Platform } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { AutoDragSortableView } from "react-native-drag-sort";
import LatItemCollections from "./LatItemCollections";

import { editListCollection } from "store/slices/categorySlice";

import CollectionSortItem from "views/Scores/components/CollectionSortItem";
import { IContentItemLesson } from "types/Content";
import { useDispatch, useSelector } from "react-redux";
import { fetchContentListItem, selectContentItem } from "store/slices/contentSlice";


interface ScreenCollectionProps {
  contentCategories?: any;
  style?: any;
  onAddCollections: any;
  bottomSheetAddPhotoRef: any;
  heightFilterBar?: any;
  filtering?: any;
  isEditCollection?: boolean;
  isShowEditLocation?: boolean;
  onItemSwipe?: (uuid: string, swipeRow: any) => void;
  onShareCateGoryCollectionPressed: (id: number, instanceId: string | null) => void;
  onPressEditCollection?: () => void;
  refreshList:()=>void;
  onListScrollEnabled?: (enabled: boolean) => void;
  setIsShowEditLocation?: any;
}

const CollectionsSort = (props: ScreenCollectionProps) => {
  const {
    style,
    onAddCollections,
    bottomSheetAddPhotoRef,
    heightFilterBar,
    filtering,
    contentCategories,
    onItemSwipe,
    isEditCollection,
    onPressEditCollection,
    onListScrollEnabled,
    refreshList,
    onShareCateGoryCollectionPressed,
  } = props;

  const dispatch = useDispatch();
  const { width: screenWidth, height: screenHeight } = useWindowDimensions();

  const childrenWidth = screenWidth;
  const parentWidth = screenWidth;
  const childrenHeight = 48;
  const renderItem = React.useCallback(
    (items: any) => {
      console.log("items",items)
      return (
        <CollectionSortItem
          uuid={items.uuid}
          onItemSwipe={onItemSwipe}
          onListScrollEnabled={onListScrollEnabled}
          isEditCollection={isEditCollection}
          style={style}
        />
      );
    },
    [isEditCollection, style, onItemSwipe, onListScrollEnabled]
  );
// const onSortCollection  = () => {
 
//   const contentItem: IContentItemLesson = useSelector((state) =>
         
//   selectContentItem(state, data[0]?.uuid)
// );
// console.log("contentItem",contentItem)
// }
  return (
    <SafeAreaView
      style={{ flex: 1, marginTop: Platform.OS === "ios" ? -35 : 10 }}
    >
      <View
        style={{
          height:
            Platform.OS === "ios" ? screenHeight / 1.75 : screenHeight / 1.65,
        }}
      >
        <AutoDragSortableView
          dataSource={contentCategories || []}
          parentWidth={parentWidth}
          childrenWidth={childrenWidth}
          childrenHeight={childrenHeight}
          marginChildrenBottom={24}
          scaleStatus={"scaleY"}
          // onDragStart={()=>  setIsScroll(false)}
          // onDragEnd={() => setIsScroll(true)}

          onDataChange={(data) => {
            let listId: any[] = []
            data.map((item) => {
              listId.push(item?.uuid);
            });
            const dataEdit = {
              lesson_ids: listId,
              category_id: filtering?.id,
            };
            console.log("dataEdit", dataEdit)

            //@ts-ignore
            dispatch(editListCollection(dataEdit));
            setTimeout(() =>{refreshList()},1000)

          }}
          keyExtractor={(item, index) => item.id} // FlatList
          renderItem={renderItem}
          renderBottomView={
            filtering?.lv2Id?
            <View style={{marginTop:10}}>
            <LatItemCollections
              onEdit={onPressEditCollection}
              onAddCollections={onAddCollections}
              bottomSheetAddPhotoRef={bottomSheetAddPhotoRef}
              style={{ marginBottom: heightFilterBar + 10 }}
              onShareCateGoryCollectionPressed={
                onShareCateGoryCollectionPressed
              }
            /> 
            </View> : null
          }
        />
      </View>
    </SafeAreaView>
  );
};
export default CollectionsSort;
