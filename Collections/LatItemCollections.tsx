import { getIcons } from 'app/icons';
import { blueColor, fontRegular } from 'assets/styles/theme';
import React from 'react';
import { TouchableOpacity, Text, StyleSheet, View, Keyboard, Platform } from 'react-native'
import { useTheme } from 'react-native-paper';
import { check, PERMISSIONS, request, RESULTS } from "react-native-permissions";
import { useSharedValue, withTiming } from 'react-native-reanimated';
import DocumentPicker from 'react-native-document-picker';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { selectFiltering } from 'store/slices/scoresSlice';

interface LastCollections {
  style?: any;
  onShareCateGoryCollectionPressed:  (id: number, instanceId: string | null) => void;
  bottomSheetAddPhotoRef?: any;
  onAddCollections: any;
  groupedScoresList?: any
  onEdit?: () => void;
  isShowEditLocation?: boolean,
  setIsShowEditLocation?: any,
  isPreviewList?: boolean,
}

const IconAddPhoto = getIcons("IconAddPhotoCollection");
const IconAddFile = getIcons("IconAddFileCollection");
const IconShare = getIcons("IconShareFileCollection");
const IconEdit = getIcons("Icon_Edit_collection");



const LatItemCollections = (props: LastCollections) => {
  const { onAddCollections, style, bottomSheetAddPhotoRef, onEdit, isPreviewList,onShareCateGoryCollectionPressed } = props
  const theme = useTheme();
  const styles = makeStyles(theme);
  const filtering = useSelector(selectFiltering);
  const { t } = useTranslation();
  const opacityInput = useSharedValue(1);
  const [fileSelect, setFileSelect] = React.useState({
    name: "",
    path: "",
    type: "",
  });
  const onAttachmentButtonPressed = async () => {
    Keyboard.dismiss();
    const permission =
      Platform.OS === "ios"
        ? PERMISSIONS.IOS.PHOTO_LIBRARY
        : PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE;
    opacityInput.value = withTiming(0);
    try {
      const checkPermission = await check(permission);
      if (checkPermission !== RESULTS.GRANTED) {
        await request(permission);
      }
    } catch (error) {
      console.log("---------------------------");
      console.log("Error check and request permission", error);
      console.log("---------------------------");
    }
    bottomSheetAddPhotoRef?.current?.expand();
  };


  const onPickerFile = async () => {
    try {
      const res = await DocumentPicker.pickSingle({
        type: [
          DocumentPicker.types.allFiles,

        ],
        //There can me more options as well
        // DocumentPicker.types.allFiles

        // DocumentPicker.types.plainText
        // DocumentPicker.types.audio
        // DocumentPicker.types.pdf

      });
      setFileSelect({ path: res.uri, name: res.name, type: res.type })



    } catch (err) {
      //Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        //If user canceled the document selection
        alert('Canceled from single doc picker');

      } else {
        //For Unknown Error
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  }
  React.useEffect(() => {
    const info = {
      title:fileSelect?.name,
      composer:""
    }

    if (fileSelect?.name.length) {
      onAddCollections(1, 1, info, fileSelect?.path, fileSelect?.name, fileSelect?.type);
      setFileSelect({ path: "", name: "", type: "" })
    }
  }, [fileSelect, onAddCollections])
  const onBtnSharePressedCallback = React.useCallback(() => {
    onShareCateGoryCollectionPressed && onShareCateGoryCollectionPressed(filtering?.lv2Id,"");

}, [onShareCateGoryCollectionPressed,filtering?.lv2Id]);
  return (
    <View>
      {/* {isPreviewList && (groupedScoresList?.length ? (null) : (<View style={{ flex: 1, height: Platform.OS ==="ios"?screenHeight /3:screenHeight/2.7 }}></View>))} */}
      <View style={[styles.container, isPreviewList? style:{marginTop:0},]}>

        <View style={styles.containerChildren}>
          <TouchableOpacity onPress={onAttachmentButtonPressed} style={styles.item} >
            <IconAddPhoto />
            <Text style={styles.textLastItem}>{t('colecction.add_photo')}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.item, { marginLeft: 15 }]} onPress={() => onPickerFile()}>
            <IconAddFile />
            <Text style={styles.textLastItem}>{t('collection.add_file')}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.containerChildren}>
          <TouchableOpacity onPress = {onBtnSharePressedCallback} style={[styles.item, { marginRight: 15 }]}>
            <IconShare />
            <Text style={styles.textLastItem}>{t('collection.share')}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={onEdit} style={[styles.item]}>
            <IconEdit />
            <Text style={styles.textLastItem}>{t('collection.edit')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}
export default LatItemCollections;
// eslint-disable-next-line no-undef
const makeStyles = (theme: ReactNativePaper.Theme) =>
  StyleSheet.create({
    container: {
      flexDirection: "row",
      alignItems: "flex-end",
      justifyContent: "space-between",
      marginTop: 10,
      marginBottom: 20,
      paddingHorizontal: 16,
    },
    textLastItem: {
      fontSize: 18,
      color: blueColor[0],
      marginTop: 5,
      fontFamily:fontRegular,
    },
    item: {
      flexDirection: "column",
      minWidth: 60,
      alignItems: "center",
    },
    containerChildren: {
      flexDirection: "row",
      alignItems: "center",
    },
    cardCollections: {
      backgroundColor: theme.colors.white,
      height: 64,
      marginTop: 9,
    },
    thumbnailPdf: { width: 70, height: 70 },
    content: {

      paddingHorizontal: 12,
      paddingVertical: 12,
    },
    wrapperBtnClose: { marginLeft: 20, marginBottom: 10, height: 24 },



  });

