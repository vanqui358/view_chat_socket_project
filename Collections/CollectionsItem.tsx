import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import makeStyles from "assets/styles/components/Card/scoreCardStyles";

// images
import IconTagGuitar from "assets/images/svg/IconTagGuitar.svg";
import IconTagPiano from "assets/images/svg/IconTagPiano.svg";
import IconTagSaxo from "assets/images/svg/IconTagSaxo.svg";
import IconTagViolin from "assets/images/svg/IconTagViolin.svg";
import IconTagDanbau from "assets/images/svg/IconTagDanbau.svg";
import IconFavourite1 from "assets/images/svg/IconFavourite1.svg";
import IconFavourite0 from "assets/images/svg/IconFavourite0.svg";
import IconShare from "assets/images/svg/IconShare.svg";
import IconBlueAttach from "assets/images/svg/IconBlueAttach.svg";
import IconComment from "assets/images/svg/IconCommentColorCustom.svg";
// import IconAttachMini from "assets/images/svg/IconAttachMini.svg";
// import IconFavouriteMini from "assets/images/svg/IconFavouriteMini.svg";
import IconAttach0 from "assets/images/svg/IconAttach0.svg";
import IconAttach1 from "assets/images/svg/IconAttach1.svg";
import IconEditList from 'assets/images/svg/IconEditList.svg';
import Icon from "react-native-vector-icons/FontAwesome5";
import SwipeableButton from 'components/Button/SwipeableButton';
import VerticalLine from 'components/VerticalLine/VerticalLine';
import FileIcon from 'components/ViewFile/FileIcon';
import { blueColor, grayColor, ScoreLevelColor } from "assets/styles/theme";
import ContentCard from 'components/Card/ContentCard';
import { useTheme } from 'react-native-paper';
import { useTranslation } from 'react-i18next';
import {
    getListContentCategories
} from "store/slices/categorySlice";
import { useDispatch } from 'react-redux';
import { useIAP } from 'react-native-iap';
import { SCREENS } from 'app/constants';
import { useNavigation } from '@react-navigation/native';

interface Collections {
    items: any;
    contentCategories?: any;
    onTapItemCollection?: (file: string | null) => void,
    favorite: boolean;
    style?: any;
    onCardPress: (
        id: number,
        title: string,
        composer: string,
        instanceId: string | null
    ) => void;
    filtering: any
    onFavoriteCollection?: (params: any, favorite: boolean) => void;
    onCommentPress: (id: number, instanceId: string | null) => void;
    onAttachPressed: (id: number, instanceId: string | null) => void;
    onShareCollectionPressed: (id: number, instanceId: string | null) => void;

    isShowEdit: boolean | undefined;
    level: number;
}
const CollectionItem = (props: Collections) => {
    const {
        items,
        onTapItemCollection,
        style,
        onCardPress,
        onFavoriteCollection,
        onCommentPress,
        isShowEdit,
        onShareCollectionPressed,
        favorite,
        level,
        onAttachPressed
    } = props;

    const [favorited, setFavorited] = React.useState<boolean>(favorite);
    const { currentPurchase, } = useIAP();
    const { width } = Dimensions.get('window');
    const childrenWidth = width;
    const navigation = useNavigation();
    const { t } = useTranslation();
    const theme = useTheme();
    const styles = React.useMemo<any>(() => makeStyles(theme), [theme]);
    const levelBgStyle = React.useMemo(
        () => ({
            backgroundColor:
                ScoreLevelColor[level > 0 && level <= 12 ? level - 1 : 0],
        }),
        [level]
    );
    const onCardPressedCallback = React.useCallback((id, type, name, composer, instanceId, file, file_type) => {
        if (items?.subscription_status === 1) {
            if (currentPurchase !== undefined) {

                if (type === 3) {
                    onTapItemCollection(file, file_type)
                }
                else {
                    onCardPress && onCardPress(id, name, composer, instanceId);
                }
            } else {
                navigation.navigate(SCREENS.PremiumSubscription, {type:"premium"});
            }
        } else {
            if (type === 3) {
                onTapItemCollection(file, file_type)
            }
            else {
                onCardPress && onCardPress(id, name, composer, instanceId);
            }
        }
    }, [items?.subscription_status, currentPurchase, onTapItemCollection, onCardPress, navigation]);


    const onFavoriteCollectionOnPress = (id: string) => {
        const params = {
            collection_id: id,
            data: { type: 3 },
        }
        setFavorited((v) => !v)
        onFavoriteCollection && onFavoriteCollection(params, !favorited);

    }

    const onBtnCommentPressedCallback = React.useCallback((id, instanceId) => {
        onCommentPress && onCommentPress(id, instanceId);
    }, [onCommentPress]);



    const onBtnSharePressedCallback = React.useCallback((id, instanceId) => {
        onShareCollectionPressed && onShareCollectionPressed(id, instanceId);

    }, [onShareCollectionPressed]);

    const { hasAttachment, canAttach, IcAttach } = React.useMemo(() => {
        let hasAttachment = false;
        let canAttach = false;
        let IcAttach = IconAttach0;
        switch (items.attachable) {
            case 1:
                IcAttach = IconBlueAttach;
                canAttach = true;
                break;
            case 2:
                IcAttach = IconAttach1;
                hasAttachment = true;
                canAttach = true;
                break;
            default:
                break;
        }
        return {
            hasAttachment,
            canAttach,
            IcAttach,
        };
    }, [items?.attachable]);
    const onBtnAttachPressedCallback = React.useCallback(() => {
        if (!canAttach) return;
        onAttachPressed && onAttachPressed(items?.score?.id, items?.score?.user_score_id);
    }, [items?.score?.id, canAttach, onAttachPressed, items?.score?.user_score_id]);

    return (
        <View style={{ flexDirection: "row" }}>
            <View style={{ backgroundColor: "#333", width: "100%", maxWidth: isShowEdit ? width / 1.2 : width }}>
                <ContentCard
                    title={items?.name}
                    Icon={(props: { style: any }) => (
                        <FileIcon typeName={items?.file} typeFile={items?.file_type} type={items?.type} typeList="collection" />
                    )}
                    subtitle={items?.score?.composer || items?.composer || ""}
                    preview={false}
                    onCardPress={() => onCardPressedCallback(items?.score?.id, items?.type,
                        items?.name, items?.score?.composer, items?.score?.user_score_id,
                        items?.file, items?.file_type)}
                    style={style}
                    InfoLine1={(props: { style: any }) => (
                        <View style={props.style}>
                            <View style={styles.instrumentsWrapper}>

                                {items?.score?.instruments.map((instrument) => {
                                    let Icon = null;
                                    switch (instrument) {
                                        case 1:
                                            Icon = IconTagPiano;
                                            break;
                                        case 2:
                                            Icon = IconTagGuitar;
                                            break;
                                        case 3:
                                            Icon = IconTagSaxo;
                                            break;
                                        case 4:
                                            Icon = IconTagViolin;
                                            break;
                                        case 5:
                                            Icon = IconTagDanbau;
                                            break;
                                        default:
                                            return <></>;
                                    }
                                    return (
                                        <Icon key={instrument} style={styles.iconInstrument} />
                                    );
                                })}
                            </View>
                        </View>
                    )}
                    InfoLine2={(props: { style: any }) => (

                        <View style={props.style}>
                            {items?.type === 0 && <View style={[styles.levelWrapper, levelBgStyle]}>
                                <Text style={[styles.levelLabel]}>
                                    {t("label.score_level", { level })}
                                </Text>
                            </View>}

                        </View>
                    )}
                    SwipeActionsLeft={(props: { style: any }) => (
                        <View style={[styles.leftSectionHiddenView, props.style]}>
                            <SwipeableButton
                                onPress={() => onFavoriteCollectionOnPress(items?.id)}
                                color="#F2F2F7"
                                icon={favorited ? <IconFavourite1 /> : <IconFavourite0 />}
                            />
                            <VerticalLine />
                            {items?.type !== 3 && <SwipeableButton
                                onPress={() => onBtnCommentPressedCallback(items?.id, items?.user_score_id)}
                                color="#F2F2F7"
                                disabled={items?.notation_count === 0}
                                icon={
                                    <IconComment
                                        color={items?.notation_count === 0 ? grayColor[3] : blueColor[2]}
                                    />
                                }
                            />}

                            <VerticalLine />
                        </View>
                    )}
                    SwipeActionsRight={(props: { style: any }) => (
                        <View style={[styles.rightSectionHiddenView, props.style]}>
                            <SwipeableButton
                                onPress={() => onBtnSharePressedCallback(items?.id, items?.score?.user_score_id)}
                                color="#F2F2F7"
                                disabled={false}
                                icon={<IconShare />}
                            />
                            <VerticalLine />
                            <SwipeableButton
                                disabled={!canAttach}
                                onPress={onBtnAttachPressedCallback}
                                color="#F2F2F7"
                                icon={<IcAttach />}
                            />
                            <VerticalLine />
                        </View>
                    )}
                />
            </View>
            <View style={{
                width: childrenWidth,
                zIndex: 1,
                backgroundColor: grayColor[0]

            }}>
                <View style={{ width: 64, flex: 1, alignItems: "center", justifyContent: "center" }}>
                    <IconEditList />
                </View>
            </View>
        </View>
    )
}

export default CollectionItem;